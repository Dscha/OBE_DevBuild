class CfgPatches
{
	class DS_OBE_Layer
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Soft_F"};
	};
};



/////////////////////////////////////////////////////
////////////////// LAYER DATA ///////////////////////
/////////////////////////////////////////////////////

class CfgOBE
{
	class C_Hatchback_01_F
	{
		positions[] =
		{
			{-0.929199,0.090332,-0.248512},		//STR_OBE_VehPos_Driver
			{0.854492,-0.00341797,-0.256302},	//CoDriver
			{0.897949,-1.16919,-0.197901},		//Back Right
			{-0.984375,-1.06177,-0.183472}		//Back Left
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},			"STR_OBE_VehPos_CoDriver"},
			{"crg",		"getincargo_ca",	{1},			"STR_OBE_VehPos_Backseat_Right"},
			{"crg",		"getincargo_ca",	{2},			"STR_OBE_VehPos_Backseat_Left"}
		};
	};
	class C_Hatchback_01_beigecustom_F: C_Hatchback_01_F{};
	class C_Hatchback_01_black_F: C_Hatchback_01_F{};
	class C_Hatchback_01_blue_F: C_Hatchback_01_F{};
	class C_Hatchback_01_bluecustom_F: C_Hatchback_01_F{};
	class C_Hatchback_01_dark_F: C_Hatchback_01_F{};
	class C_Hatchback_01_green_F: C_Hatchback_01_F{};
	class C_Hatchback_01_grey_F: C_Hatchback_01_F{};
	class C_Hatchback_01_sport_blue_F: C_Hatchback_01_F{};
	class C_Hatchback_01_sport_F: C_Hatchback_01_F{};
	class C_Hatchback_01_sport_green_F: C_Hatchback_01_F{};
	class C_Hatchback_01_sport_grey_F: C_Hatchback_01_F{};
	class C_Hatchback_01_sport_orange_F: C_Hatchback_01_F{};
	class C_Hatchback_01_sport_red_F: C_Hatchback_01_F{};
	class C_Hatchback_01_sport_white_F: C_Hatchback_01_F{};
	class C_Hatchback_01_white_F: C_Hatchback_01_F{};
	class C_Hatchback_01_yellow_F: C_Hatchback_01_F{};
	
	
	class C_Van_01_fuel_F
	{
		positions[] =
		{
			{-1.09131,0.498047,-0.261139},	//Driver
			{1.0459,0.433105,-0.260544}		//CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0,1},			"STR_OBE_VehPos_CoDriver"}
		};
	};
	class I_G_Van_01_fuel_F: C_Van_01_fuel_F {};
	class B_G_Van_01_fuel_F: C_Van_01_fuel_F {};
	class O_G_Van_01_fuel_F: C_Van_01_fuel_F {};
	class C_Van_01_fuel_white_F: C_Van_01_fuel_F {};
	class C_Van_01_fuel_white_v2_F: C_Van_01_fuel_F {};
	class C_Van_01_fuel_red_F: C_Van_01_fuel_F {};
	class C_Van_01_fuel_red_v2_F: C_Van_01_fuel_F {};
	class C_Van_01_box_F: C_Van_01_fuel_F {};
	class C_Van_01_box_white_F: C_Van_01_fuel_F {};
	class C_Van_01_box_red_F: C_Van_01_fuel_F {};
	
	
	class C_Van_01_transport_F
	{
		positions[] =
		{
			{-0.90332,0.473877,-0.286118},	//Driver
			{0.880859,0.424805,-0.283104},	//CoDriver
			{-0.996094,-1.28784,-0.316803},	//Seats (Left)
			{-0.969238,-3.42773,-0.362305},	//Backseats (Left)
			{0.928223,-3.42749,-0.366653},	//Backseats (Right)
			{0.944336,-1.1521,-0.334473}	//Seats (Right)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0,1},			"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getincargo_ca",	{0,1,2},		"STR_OBE_VehPos_Seats_Left"},
			{"trt",		"getincargo_ca",	{3,4},			"STR_OBE_VehPos_Backseats_Left"},
			{"trt",		"getincargo_ca",	{8,9},			"STR_OBE_VehPos_Backseats_Right"},
			{"trt",		"getincargo_ca",	{5,6,7},		"STR_OBE_VehPos_Seats_Right"}
		};
	};
	class I_G_Van_01_transport_F: C_Van_01_transport_F {};
	class I_C_Van_01_transport_F: C_Van_01_transport_F {};
	class B_G_Van_01_transport_F: C_Van_01_transport_F {};
	class O_G_Van_01_transport_F: C_Van_01_transport_F {};
	class C_Van_01_transport_white_F: C_Van_01_transport_F {};
	class C_Van_01_transport_red_F: C_Van_01_transport_F {};
	class I_C_Van_01_transport_olive_F: C_Van_01_transport_F {};
	
	class C_Kart_01_F
	{
		positions[] =
		{
			{0.00000,-0.0769043,-0.480809}	//Driver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"}
		};
	};
	class C_Kart_01_Fuel_F: C_Kart_01_F{};
	class C_Kart_01_green_F: C_Kart_01_F{};
	class C_Kart_01_orange_F: C_Kart_01_F{};
	class C_Kart_01_white_F: C_Kart_01_F{};
	class C_Kart_01_yellow_F: C_Kart_01_F{};
	class C_Kart_01_Blu_F: C_Kart_01_F{};
	class C_Kart_01_Red_F: C_Kart_01_F{};
	class C_Kart_01_Vrana_F: C_Kart_01_F{};
	
	
	class C_Offroad_02_unarmed_F
	{
		positions[] =
		{
			{-0.686035,0.174561,-0.629017},		//Driver
			{0.599121,0.174805,-0.631239},		//CoDriver
			{0.803223,-0.887207,-0.26886},		//Back Right
			{-0.890625,-0.86792,-0.280914}		//Back Left
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"},
			{"trt",		"getincargo_ca",	{0},			"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getincargo_ca",	{1},			"STR_OBE_VehPos_Backseat_Right"},
			{"trt",		"getincargo_ca",	{2},			"STR_OBE_VehPos_Backseat_Left"}
		};
	};
	class I_C_Offroad_02_unarmed_F: C_Offroad_02_unarmed_F{};
	class C_Offroad_02_unarmed_white_F: C_Offroad_02_unarmed_F{};
	class C_Offroad_02_unarmed_red_F: C_Offroad_02_unarmed_F{};
	class C_Offroad_02_unarmed_orange_F: C_Offroad_02_unarmed_F{};
	class C_Offroad_02_unarmed_green_F: C_Offroad_02_unarmed_F{};
	class C_Offroad_02_unarmed_blue_F: C_Offroad_02_unarmed_F{};
	class C_Offroad_02_unarmed_black_F: C_Offroad_02_unarmed_F{};
	class I_C_Offroad_02_unarmed_olive_F: C_Offroad_02_unarmed_F{};

	class C_Offroad_01_F
	{
		positions[] =
		{
			{-1.02637,0.361816,-0.354189},	//Driver
			{0.951172,0.307861,-0.364813},	//CoDriver
			{0.609863,-2.82056,-0.376266},	//Backseat (Right)
			{-0.686035,-2.82031,-0.368639},	//Backseat (Left)
			{0.939941,-1.24731,-0.189924},	//Seat (Right)
			{-1.01904,-1.2771,-0.209839}	//Seat (Left)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},			"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getincargo_ca",	{0},			"STR_OBE_VehPos_Backseat_Right"},
			{"trt",		"getincargo_ca",	{1},			"STR_OBE_VehPos_Backseat_Left"},
			{"trt",		"getincargo_ca",	{2},			"STR_OBE_VehPos_Seat_Right"},
			{"trt",		"getincargo_ca",	{3},			"STR_OBE_VehPos_Seat_Left"}
		};
	};
	class B_GEN_Offroad_01_gen_F: C_Offroad_01_F {};
	class C_Offroad_01_white_F: C_Offroad_01_F {};
	class C_Offroad_01_sand_F: C_Offroad_01_F {};
	class C_Offroad_01_red_F: C_Offroad_01_F {};
	class C_Offroad_01_darkred_F: C_Offroad_01_F {};
	class C_Offroad_01_bluecustom_F: C_Offroad_01_F {};
	class C_Offroad_01_blue_F: C_Offroad_01_F {};
	class C_Offroad_stripped_F: C_Offroad_01_F {};
	class C_Offroad_luxe_: C_Offroad_01_F {};
	class I_G_Offroad_01_F: C_Offroad_01_F {};
	class O_G_Offroad_01_F: C_Offroad_01_F {};
	class B_G_Offroad_01_F: C_Offroad_01_F {};
	
	
	class C_Offroad_01_repair_F	//Offroad - 1 Backseats only
	{
		positions[] =
		{
			{-1.03369,0.352539,-0.368225},	//Driver
			{0.938477,0.316406,-0.406174},	//CoDriver
			{-0.991699,-2.43408,-0.183632},	//Backseat Left
			{0.00000,-2.70337,-0.715771},	//Backseat (both) from Center
			{0.904297,-2.43408,-0.183632}	//Backseat Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},			"STR_OBE_VehPos_CoDriver"},
			{"crg",		"getincargo_ca",	{2},			"STR_OBE_VehPos_Backseat_Left"},
			{"crg",		"getincargo_ca",	{1,2},			"STR_OBE_VehPos_Backseats_Center"},
			{"crg",		"getincargo_ca",	{1},			"STR_OBE_VehPos_Backseat_Right"}
		};
	};
	class I_G_Offroad_01_repair_F: C_Offroad_01_repair_F {};
	class O_G_Offroad_01_repair_F: C_Offroad_01_repair_F {};
	class B_G_Offroad_01_repair_F: C_Offroad_01_repair_F {};
	
	
	class B_G_Offroad_01_armed_F	//Offroad + MG Turret
	{
		positions[] =
		{
			{-1.01465,0.479736,-0.752247},	//Driver
			{0.925781,0.388428,-0.751438},	//CoDriver
			{0.916992,-1.99487,-0.68471},	//Gunner from Right
			{-0.0332031,-2.55273,-1.26637},	//Gunner from Back
			{-1.00293,-2.0271,-0.729687}	//Gunner from Left
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"}
		};
	};
	class O_G_Offroad_01_armed_F: B_G_Offroad_01_armed_F {};
	class I_G_Offroad_01_armed_F: B_G_Offroad_01_armed_F {};
	
	
	class C_SUV_01_F
	{
		positions[] =
		{
			{-0.991699,0.000244141,-0.195403},	//Driver
			{0.981445,-0.0664063,-0.203137},	//CoDriver
			{-0.998047,-1.01392,-0.180681},		//Back Left
			{0.988281,-1.04028,-0.195256}		//Back Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_CoDriver"},
			{"crg",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Backseat_Left"},
			{"crg",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Backseat_Right"}
		};
	};
	
	
	class C_Truck_02_fuel_F		//Kamaz - Fuel
	{
		positions[] =
		{
			{-1.12891,2.85986,-0.469513},	//Driver
			{1.07178,2.85229,-0.465919}	//CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0,1},	"STR_OBE_VehPos_CoDriver"}
		};
	};
	class I_Truck_02_fuel_F: C_Truck_02_fuel_F{};
	class O_Truck_02_fuel_F: C_Truck_02_fuel_F{};
	
	class C_Truck_02_box_F	//Kamaz - Box
	{
		positions[] =
		{
			{-1.12793,3.03442,-0.466928},	//Driver
			{1.07227,3.03027,-0.471634}		//CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0,1},	"STR_OBE_VehPos_CoDriver"}
		};
	};
	class I_Truck_02_box_F: C_Truck_02_box_F{};
	class O_Truck_02_box_F: C_Truck_02_box_F{};
	class I_Truck_02_ammo_F: C_Truck_02_box_F{};
	class O_Truck_02_Ammo_F: C_Truck_02_box_F{};
	
	class C_Truck_02_covered_F	//Kamaz - Covered
	{
		positions[] =
		{
			{-1.0957,2.85449,-0.465073},		//Driver
			{1.10449,2.85449,-0.465073},		//CoDriver
			{-0.922363,-3.73291,-0.499298},		//Gunner (Left)
			{-0.324219,-3.73291,-0.499298},		//Backseats (Left)
			{0.949219,-3.73291,-0.499298},		//Gunner (Right)
			{0.365723,-3.73291,-0.499298}		//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},					"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0,1},				"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getincargo_ca",	{0},				"STR_OBE_VehPos_Gunner_Left"},
			{"crg",		"getincargo_ca",	{12,10,8,6,4,2},	"STR_OBE_VehPos_Backseats_Left"},
			{"trt",		"getincargo_ca",	{1},				"STR_OBE_VehPos_Gunner_Right"},
			{"crg",		"getincargo_ca",	{13,11,9,7,5,3},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	class I_Truck_02_covered_F: C_Truck_02_covered_F{};
	class O_Truck_02_covered_F: C_Truck_02_covered_F{};
	//Kamaz - Open
	class C_Truck_02_transport_F: C_Truck_02_covered_F{};
	class I_Truck_02_transport_F: C_Truck_02_transport_F{};
	class O_Truck_02_transport_F: C_Truck_02_transport_F{};
	
	class I_Truck_02_medical_F	//Kamaz - Medic
	{
		positions[] =
		{
			{-1.12842,2.8186,-0.458015},		//Driver
			{1.07129,2.81616,-0.451797},		//STR_OBE_VehPos_CoDriver
			{-0.776367,-3.73242,-0.488487},		//Back Left
			{0.704102,-3.73242,-0.473465}		//Back Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},						"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0,1},					"STR_OBE_VehPos_CoDriver"},
			{"crg",		"getincargo_ca",	{2,3,4,5,6,7,8},		"STR_OBE_VehPos_Backseats_Left"},
			{"crg",		"getincargo_ca",	{9,10,11,12,13,14,15},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	class O_Truck_02_medical_F: I_Truck_02_medical_F{};
	
	
	class B_Truck_01_mover_F	//HEMTT Mover
	{
		positions[] =
		{
			{-1.18018,3.31909,-0.130468},		//Driver
			{1.23584,3.32422,-0.131847}		//STR_OBE_VehPos_CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_CoDriver"}
		};
	};
	class B_T_Truck_01_mover_F: B_Truck_01_mover_F{};
	
	class B_Truck_01_ammo_F	//HEMTT Ammo
	{
		positions[] =
		{
			{-1.17969,4.12061,-0.13943},		//Driver
			{1.23584,4.15527,-0.156925}		//STR_OBE_VehPos_CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_CoDriver"}
		};
	};
	class B_T_Truck_01_ammo_F: B_Truck_01_ammo_F{};
	
	class B_Truck_01_fuel_F	//HEMTT Fuel
	{
		positions[] =
		{
			{-1.17969,3.92773,-0.141098},		//Driver
			{1.23584,3.95703,-0.129504}		//STR_OBE_VehPos_CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_CoDriver"}
		};
	};
	class B_T_Truck_01_fuel_F: B_Truck_01_fuel_F{};
	
	class B_Truck_01_Repair_F	//HEMTT Repair
	{
		positions[] =
		{
			{-1.20361,4.27026,-0.517739},		//Driver
			{1.2124,4.26611,-0.536106}		//STR_OBE_VehPos_CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_CoDriver"}
		};
	};
	class B_T_Truck_01_Repair_F: B_Truck_01_Repair_F{};
	
	
	class B_Truck_01_medical_F	//HEMTT Medic
	{
		positions[] =
		{
			{-1.35303,4.21973,-0.415569},		//Driver
			{1.06299,4.2207,-0.42767},			//STR_OBE_VehPos_CoDriver
			{-0.966309,-4.97192,-0.281738},		//Back Left
			{0.670898,-4.97192,-0.273994}		//Back Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},							"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},						"STR_OBE_VehPos_CoDriver"},
			{"crg",		"getincargo_ca",	{1,2,3,4,5,6,7},			"STR_OBE_VehPos_Backseats_Left"},
			{"crg",		"getincargo_ca",	{8,9,10,11,12,13,14,15},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	class B_T_Truck_01_medical_F: B_Truck_01_medical_F{};
	
	
	class B_Truck_01_transport_F	//HEMTT Open
	{
		positions[] =
		{
			{-1.17383,4.12964,-0.386137},		//Driver
			{1.24219,4.10693,-0.397892},		//STR_OBE_VehPos_CoDriver
			{-0.901367,-5.00977,-0.316917},		//Gunner (Left)
			{-0.342285,-5.00977,-0.316917},		//Backseats (Left)
			{0.953125,-5.00977,-0.316917},		//Gunner (Right)
			{0.376953,-5.00977,-0.316917}		//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},						"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},					"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getincargo_ca",	{0},					"STR_OBE_VehPos_Gunner_Left"},
			{"crg",		"getincargo_ca",	{6,5,4,3,2,14,1},		"STR_OBE_VehPos_Backseats_Left"},
			{"trt",		"getincargo_ca",	{1},					"STR_OBE_VehPos_Gunner_Right"},
			{"crg",		"getincargo_ca",	{13,12,11,10,9,8,7},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	class B_T_Truck_01_transport_F: B_Truck_01_transport_F{};
	//HEMTT Covered
	class B_Truck_01_covered_F: B_Truck_01_transport_F{};
	class B_T_Truck_01_covered_F: B_Truck_01_transport_F{};
	
	//Tempest
	class O_Truck_03_device_F	//Tempest Device
	{
		positions[] =
		{
			{-1.2666,2.58179,0.056778},		//Driver
			{1.26367,2.54663,0.0876083}		//STR_OBE_VehPos_CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},					"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},				"STR_OBE_VehPos_CoDriver"}
		};
	};
	class O_T_Truck_03_device_ghex_F: O_Truck_03_device_F{};
	//Tempest Ammo
	class O_Truck_03_ammo_F: O_Truck_03_device_F{};
	class O_T_Truck_03_ammo_ghex_F: O_Truck_03_ammo_F{};
	
	class O_Truck_03_fuel_F	//Tempest Fuel
	{
		positions[] =
		{
			{-1.2666,2.64282,0.117161},		//Driver
			{1.26367,2.64819,0.123265}		//STR_OBE_VehPos_CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},					"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},				"STR_OBE_VehPos_CoDriver"}
		};
	};
	class O_T_Truck_03_fuel_ghex_F: O_Truck_03_fuel_F{};
	
	class O_Truck_03_repair_F	//Tempest Repair
	{
		positions[] =
		{
			{-1.26611,2.57056,0.115646},	//Driver
			{1.26367,2.57788,0.120262}		//STR_OBE_VehPos_CoDriver
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},					"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},				"STR_OBE_VehPos_CoDriver"}
		};
	};
	class O_T_Truck_03_repair_ghex_F: O_Truck_03_repair_F{};
	
	class O_Truck_03_medical_F	//Tempest Medic
	{
		positions[] =
		{
			{-1.19287,2.85376,-0.146065},	//Driver
			{1.3374,2.8479,-0.143044},		//STR_OBE_VehPos_CoDriver
			{-0.678711,-5.0293,-0.197914},	//Back Left
			{0.814453,-5.0293,-0.193733}	//Back Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},					"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},				"STR_OBE_VehPos_CoDriver"},
			{"crg",		"getincargo_ca",	{5,4,3,2,1},		"STR_OBE_VehPos_Backseats_Left"},
			{"crg",		"getincargo_ca",	{10,9,11,8,7,6},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	class O_T_Truck_03_medical_ghex_F: O_Truck_03_medical_F{};
	
	class O_Truck_03_transport_F	//Tempest Open
	{
		positions[] =
		{
			{-1.21533,2.84546,-0.140251},		//Driver
			{1.31543,2.85229,-0.136086},		//STR_OBE_VehPos_CoDriver
			{-0.928223,-5.01953,-0.176407},		//Gunner (Left)
			{-0.341797,-5.01953,-0.176407},		//Backseats (Left)
			{1.05957,-5.01953,-0.176407},		//Gunner (Right)
			{0.459473,-5.01953,-0.176407}		//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},			"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getincargo_ca",	{0},			"STR_OBE_VehPos_Gunner_Left"},
			{"crg",		"getincargo_ca",	{4,3,2,8,1},	"STR_OBE_VehPos_Backseats_Left"},
			{"trt",		"getincargo_ca",	{1},			"STR_OBE_VehPos_Gunner_Right"},
			{"crg",		"getincargo_ca",	{9,10,7,6,5},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	class O_T_Truck_03_transport_ghex_F: O_Truck_03_transport_F{};
	//Tempest Covered
	class O_Truck_03_covered_F: O_Truck_03_transport_F{};
	class O_T_Truck_03_covered_ghex_F: O_Truck_03_transport_F{};
	
	

	class B_LSV_01_unarmed_F	//Prowler - Unarmed
	{
		positions[] =
		{
			{-1.01074,0.476318,-0.885637},		//Driver
			{-1.0083,-0.376465,-0.883131},		//Left - Seat
			{1.0791,-0.40918,-0.886259},		//Right - Seat
			{1.08154,0.473877,-0.88657},		//STR_OBE_VehPos_CoDriver
			{0.473145,-2.17578,-0.717787},		//Right - Backseat
			{-0.432129,-2.17578,-0.713467},		//Left - Backseat
			{-0.984863,-0.0700684,-0.357967},	//Center from Left
			{1.05908,-0.0537109,-0.414619}		//Center from Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Seat_Left"},
			{"trt",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Seat_Right"},
			{"trt",		"getincargo_ca",	{2},	"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getincargo_ca",	{3},	"STR_OBE_VehPos_Backseat_Right"},
			{"trt",		"getincargo_ca",	{4},	"STR_OBE_VehPos_Backseat_Left"},
			{"trt",		"getincargo_ca",	{5},	"STR_OBE_VehPos_Gunner_Center"},
			{"trt",		"getincargo_ca",	{5},	"STR_OBE_VehPos_Gunner_Center"}
		};
	};
	class B_T_LSV_01_unarmed_F: B_LSV_01_unarmed_F {};
	class B_CTRG_LSV_01_light_F: B_LSV_01_unarmed_F {};
	

	class B_LSV_01_armed_F	//Prowler - Armed
	{
		positions[] =
		{
			{-1.01074,0.477539,-0.885248},		//Driver
			{-0.984863,-0.0688477,-0.362186},	//Center from Left
			{1.05908,-0.0532227,-0.412464},		//Center from Right
			{1.08154,0.475098,-0.883781},		//Commander
			{-1.0083,-0.375977,-0.88686},		//Left - Seat
			{1.0791,-0.406494,-0.885445}		//Right - Seat
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},	//Gunner from Left
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},	//Gunner from Right
			{"trt",		"getincommander_ca",{1},	"STR_OBE_VehPos_Commander"},
			{"trt",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Seat_Left"},
			{"trt",		"getincargo_ca",	{3},	"STR_OBE_VehPos_Seat_Right"}
		};
	};
	class B_T_LSV_01_armed_F: B_LSV_01_armed_F {};
	

	class O_LSV_02_unarmed_F	//Qilin - Unarmed
	{
		positions[] =
		{
			{-0.812988,0.992676,-0.355126},	//Driver from Left
			{0.538574,0.981934,-0.359641},	//Driver from Right
			{-0.891602,-0.536133,0.223835},	//Center from Left
			{0.414551,-0.522705,0.223635},	//Center from Right
			{0.84375,0.19873,-0.58427},		//Right - Seat
			{-1.16748,0.196289,-0.576675},	//Left - Seat
			{0.84375,-0.628906,-0.53091},	//Right - Backseat
			{-1.16748,-0.72876,-0.519846},	//Left - Backseat
			{0.305664,-2.36938,-0.386218}	//Backseat
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},	//Driver from Left
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},	//Driver from Right
			{"trt",		"getincargo_ca",	{5},	"STR_OBE_VehPos_Gunner_Center"},	//Center from Left
			{"trt",		"getincargo_ca",	{5},	"STR_OBE_VehPos_Gunner_Center"},	//Center from Right
			{"trt",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Seat_Right"},	//Right - Seat
			{"trt",		"getincargo_ca",	{3},	"STR_OBE_VehPos_Seat_Left"},	//Left - Seat
			{"trt",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Backseat_Right"},	//Right - Backseat
			{"trt",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Backseat_Left"},	//Left - Backseat
			{"trt",		"getincargo_ca",	{4},	"STR_OBE_VehPos_Backseat"}		//Backseat
		};
	};
	class O_T_LSV_02_unarmed_F: O_LSV_02_unarmed_F {};
	

	class O_LSV_02_armed_F	//Qilin - Armed
	{
		positions[] =
		{
			{-0.810059,0.992432,-0.355103},	//Driver from Left
			{0.53418,0.977783,-0.360692},	//Driver from Right
			{-0.883301,-0.541504,0.223915},	//Gunner from Left
			{0.407715,-0.519287,0.223582},	//Gunner from Right
			{0.84375,0.200195,-0.580967},	//Right - Seat
			{-1.16748,0.195068,-0.581425},	//Left - Seat
			{0.84375,-0.629395,-0.529959},	//Right - Backseat
			{-1.16748,-0.729736,-0.519712},	//Left - Backseat
			{0.306641,-2.36792,-0.384548}	//Backseat
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},	//Driver from Left
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},	//Driver from Right
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},	//Gunner from Left
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},	//Gunner from Right
			{"trt",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Seat_Right"},	//Right - Seat
			{"trt",		"getincargo_ca",	{4},	"STR_OBE_VehPos_Seat_Left"},	//Left - Seat
			{"trt",		"getincargo_ca",	{3},	"STR_OBE_VehPos_Backseat_Right"},	//Right - Backseat
			{"trt",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Backseat_Left"},	//Left - Backseat
			{"trt",		"getincargo_ca",	{5},	"STR_OBE_VehPos_Backseat"}		//Backseat
		};
	};
	class O_T_LSV_02_armed_F: O_LSV_02_armed_F {};
	
	
	class C_Quadbike_01_F	//Quad
	{
		positions[] =
		{
			{0.00000,0.106201,-0.413486},	//Driver
			{0.00000,-0.663574,-0.450022}	//Backseat
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},					"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},				"STR_OBE_VehPos_Backseat"}
		};
	};
	class C_Quadbike_01_black_F: C_Quadbike_01_F{};
	class C_Quadbike_01_blue_F: C_Quadbike_01_F{};
	class C_Quadbike_01_red_F: C_Quadbike_01_F{};
	class C_Quadbike_01_white_F: C_Quadbike_01_F{};
	class B_Quadbike_01_F: C_Quadbike_01_F{};
	class B_G_Quadbike_01_F: C_Quadbike_01_F{};
	class B_T_Quadbike_01_F: C_Quadbike_01_F{};
	class O_Quadbike_01_F: C_Quadbike_01_F{};
	class O_G_Quadbike_01_F: C_Quadbike_01_F{};
	class O_T_Quadbike_01_ghex_F: C_Quadbike_01_F{};
	class I_Quadbike_01_F: C_Quadbike_01_F{};
	class I_G_Quadbike_01_F: C_Quadbike_01_F{};
	
	
	//Strider (Feneck) - Unarmed
	class I_MRAP_03_F
	{
		positions[] =
		{
			{-1.32275,0.664307,-0.222879},		//Driver from Left
			{1.31738,0.662354,-0.223549},		//Driver from Right
			{-1.2915,-0.375977,0.0381107},		//Commander
			{-1.32275,-0.237305,-0.561701},		//Backseats from Left
			{1.31738,-0.23584,-0.571966}		//Backseats from Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},	//Driver from Left
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},	//Driver from Right
			{"trt",		"getincommander_ca",	{0},	"STR_OBE_VehPos_Commander"},	//Commander
			{"crg",		"getincargo_ca",		{1},	"STR_OBE_VehPos_Backseat_Center"},	//Backseat - Left
			{"crg",		"getincargo_ca",		{0},	"STR_OBE_VehPos_Backseat_Right"}		//Backseat - Right
		};
	};
	
	//Strider (Feneck) - Armed
	class I_MRAP_03_hmg_F
	{
		positions[] =
		{
			{-1.32324,0.684326,-0.9244},	//Driver from Left
			{1.31689,0.673096,-0.922531},		//Driver from Right
			{-1.29199,-0.378906,-0.657158},	//Commander from Left
			{1.28613,-0.376221,-0.658968},	//Gunner from Right
			{-1.32324,-0.23291,-1.26572},		//Backseats from Left
			{1.31689,-0.238525,-1.26126}	//Backseats from Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},	//Driver from Left
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},	//Driver from Right
			{"trt",		"getincommander_ca",	{0},	"STR_OBE_VehPos_Commander"},	//Commander from Left
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"},	//Gunner from Left
			{"crg",		"getincargo_ca",		{0},	"STR_OBE_VehPos_Backseat"},	//Backseats from Left
			{"crg",		"getincargo_ca",		{0},	"STR_OBE_VehPos_Backseat"}		//Backseats from Right
		};
	};
	class I_MRAP_03_gmg_F: I_MRAP_03_hmg_F{};
	
	
	class B_MRAP_01_F	//Hunter - Unarmed
	{
		positions[] =
		{
			{-0.993164,-1.17261,-0.32759},		//Driver
			{1.01074,-1.12085,-0.311559},		//STR_OBE_VehPos_CoDriver
			{-0.993164,-2.24121,-0.333108},		//Back Left
			{1.01074,-2.21655,-0.2877}			//Back Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_CoDriver"},
			{"crg",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Backseat_Left"},
			{"crg",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Backseat_Right"}
		};
	};
	class B_T_MRAP_01_F: B_MRAP_01_F{};
	
	class B_MRAP_01_hmg_F	//Hunter - Armed
	{
		positions[] =
		{
			{-0.993652,-1.19434,-0.783966},		//Driver
			{1.01025,-1.19727,-0.76244},		//STR_OBE_VehPos_CoDriver
			{-0.993652,-2.22656,-0.780537},		//Gunner
			{1.01074,-2.20581,-0.756481}		//Back Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_CoDriver"},
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},
			{"crg",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Backseat_Right"}
		};
	};
	class B_MRAP_01_gmg_F: B_MRAP_01_hmg_F{};
	class B_T_MRAP_01_gmg_F: B_MRAP_01_hmg_F{};
	class B_T_MRAP_01_hmg_F: B_MRAP_01_hmg_F{};
	
	
	class O_MRAP_02_F	//Ifrit - Unarmed
	{
		positions[] =
		{
			{-1.26074,-0.973633,-0.579504},		//Driver from Left
			{1.21484,-1.02026,-0.638062},		//Driver from Right
			
			{1.25049,-2.05762,-0.460371},		//Seat (Right)
			{-1.24707,-2.04956,-0.466488},		//Seat (Left)
			{1.24316,-3.19946,-0.470965},		//Backseats (Right)
			{-1.22314,-3.21436,-0.464109}		//Backseats (Left)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Seat_Right"},
			{"crg",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Seat_Left"},
			{"crg",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Backseats_Right"},
			{"crg",		"getincargo_ca",	{3},	"STR_OBE_VehPos_Backseats_Left"}
		};
	};
	class O_T_MRAP_02_ghex_F: O_MRAP_02_F{};
	
	class O_MRAP_02_hmg_F	//Ifrit - Armed
	{
		positions[] =
		{
			{-1.21338,-1.0332,-0.979665},	//Driver from Left
			{1.21191,-1.03735,-0.98676},	//Driver from Right
			
			{1.24805,-2.03906,-0.814108},	//Gunner
			{-1.25,-2.03931,-0.810875},		//Seat (Left)
			{1.24121,-3.2041,-0.823555},	//Backseats (Right)
			{-1.24512,-3.20679,-0.817513}	//Backseats (Left)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Seat_Left"},
			{"crg",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Backseats_Right"},
			{"crg",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Backseats_Left"}
		};
	};
	class O_T_MRAP_02_hmg_ghex_F: O_MRAP_02_hmg_F{};
	class O_MRAP_02_gmg_F: O_MRAP_02_hmg_F{};
	class O_T_MRAP_02_gmg_ghex_F: O_MRAP_02_hmg_F{};
	
	
	
	class B_APC_Wheeled_01_cannon_F	//AM-7 Marshall
	{
		positions[] =
		{
			{-1.40674,1.09961,-0.56625},		//Driver
			{1.4043,-1.74414,-0.464375},		//Gunner
			{-1.39893,-1.75269,-0.509762},	//Commander
			{-0.00146484,-4.70581,-0.787506}	//Backseats
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},					"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},				"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},				"STR_OBE_VehPos_Commander"},
			{"crg",		"getincargo_ca",		{7,6,2,3,5,4,1,0},	"STR_OBE_VehPos_Backseats"}
		};
	};
	class B_T_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_cannon_F{};
	
	
	class B_APC_Tracked_01_CRV_F	//CRV 6e Bobcat
	{
		positions[] =
		{
			{-1.94189,-0.602295,-0.753801},	//Driver
			{1.94678,-1.97949,-0.744856},	//Gunner
			{-1.94189,-2.09473,-0.747337}	//Commander
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{1},	"STR_OBE_VehPos_Commander"}
		};
	};
	class B_T_APC_Tracked_01_CRV_F: B_APC_Tracked_01_CRV_F{};
	
	
	class B_APC_Tracked_01_rcws_F	//IFV 6c Panther
	{
		positions[] =
		{
			{-1.94189,-0.709229,-0.771469},	//Driver
			{1.94727,-1.98804,-0.762974},		//Gunner
			{-1.94141,-2.18311,-0.756878},	//Commander
			{0.0117188,-4.02295,-1.156}	//Backseats
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},					"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},				"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{1},				"STR_OBE_VehPos_Commander"},
			{"crg",		"getincargo_ca",		{7,6,3,2,5,4,1,0},	"STR_OBE_VehPos_Backseats"}
		};
	};
	class B_T_APC_Tracked_01_rcws_F: B_APC_Tracked_01_rcws_F{};
	
	
	class O_APC_Wheeled_02_rcws_F	//MSE-3 Marid
	{
		positions[] =
		{
			{-0.906738,-0.0449219,-0.519003},		//Driver
			{-0.905762,-0.98877,-0.502595},		//Gunner
			{-0.931641,-0.565186,-1.02833},		//Commander
			{0.308594,-4.34204,-0.81086}			//Backseats
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},					"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},				"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{1},				"STR_OBE_VehPos_Commander"},
			{"crg",		"getincargo_ca",		{7,6,3,2,5,4,1,0},	"STR_OBE_VehPos_Backseats"}
		};
	};
	class O_T_APC_Wheeled_02_rcws_ghex_F: O_APC_Wheeled_02_rcws_F{};
	
	
	class O_APC_Tracked_02_cannon_F	//BTR-K Kamysh
	{
		positions[] =
		{
			{-1.56592,0.0561523,-0.967815},		//Driver
			{-1.56836,-2.0918,-0.948988},		//Gunner
			{1.54688,-1.61035,-0.858412},		//Commander
			{-0.00439453,-4.61841,-1.03607}		//Backseats
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},					"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},				"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},				"STR_OBE_VehPos_Commander"},
			{"crg",		"getincargo_ca",		{7,6,3,2,5,4,1,0},	"STR_OBE_VehPos_Backseats"}
		};
	};
	class O_T_APC_Tracked_02_cannon_ghex_F: O_APC_Tracked_02_cannon_F{};
	
	
	class I_APC_Wheeled_03_cannon_F	//AFV-4 Gorgon
	{
		positions[] =
		{
			{-0.895996,1.229,-0.81257},		//Driver
			{-0.895508,-1.31763,-0.703009},	//Gunner
			{1.89209,-1.30811,-0.83569},		//Commander
			{0.373535,-4.31177,-1.16409}		//Backseats
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},					"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},				"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},				"STR_OBE_VehPos_Commander"},
			{"crg",		"getincargo_ca",		{7,6,3,2,5,4,1,0},	"STR_OBE_VehPos_Backseats"}
		};
	};
	
	
	class I_APC_tracked_03_cannon_F	//FV-720 Mora
	{
		positions[] =
		{
			{-1.66895,1.34277,-0.759377},		//Driver
			{-1.66943,-0.839844,-0.600113},		//Gunner
			{1.67334,-1.07959,-0.82357},		//Commander
			{-0.0166016,-3.42578,-1.06638}		//Backseats
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},					"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},				"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},				"STR_OBE_VehPos_Commander"},
			{"crg",		"getincargo_ca",		{6,3,2,5,4,1,0},	"STR_OBE_VehPos_Backseats"}
		};
	};
	
	
	class B_APC_Tracked_01_AA_F	//IFV-6a Cheetah
	{
		positions[] =
		{
			{-1.94141,-0.426758,-1.11408},	//Driver
			{1.94727,-2.05737,-1.11439},	//Gunner
			{-1.94092,-2.44409,-1.11593}	//Commander
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},	"STR_OBE_VehPos_Commander"}
		};
	};
	class B_T_APC_Tracked_01_AA_F: B_APC_Tracked_01_AA_F{};
	
	
	class O_APC_Tracked_02_AA_F	//ZSU-39 Tigris
	{
		positions[] =
		{
			{-1.56689,-0.536133,-1.18704},		//Driver
			{1.625,-2.36377,-1.12999},		//Gunner
			{-1.63916,-2.62891,-1.12877}	//Commander
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},	"STR_OBE_VehPos_Commander"}
		};
	};
	class O_T_APC_Tracked_02_AA_ghex_F: O_APC_Tracked_02_AA_F{};
	
	
	class B_MBT_01_arty_F	//M4 Scorcher
	{
		positions[] =
		{
			{-1.80566,-0.181396,-1.35078},	//Driver
			{1.86084,-2.50171,-1.34231},	//Gunner
			{-1.80566,-2.4729,-1.34912}		//Commander
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},	"STR_OBE_VehPos_Commander"}
		};
	};
	class B_T_MBT_01_arty_F: B_MBT_01_arty_F{};
	
	
	class B_MBT_01_mlrs_F	//M5 Sandstorm MLRS
	{
		positions[] =
		{
			{-1.80566,0.686279,-0.358768},	//Driver
			{1.86084,-0.85791,-0.273521}		//Gunner
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"}
		};
	};
	class B_T_MBT_01_mlrs_F: B_MBT_01_mlrs_F{};
	
	
	class B_MBT_01_cannon_F	//M2A1 - Slammer
	{
		positions[] =
		{
			{-1.80566,0.628418,-1.01148},		//Driver from Left
			{1.86084,-2.06396,-0.946257},		//Gunner
			{1.86084,-0.697266,-0.955639},		//Commander
			{-0.00683594,-3.67334,-1.22299}		//Backseats
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},				"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},			"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},			"STR_OBE_VehPos_Commander"},
			{"crg",		"getincargo_ca",		{5,2,1,3,4,0},	"STR_OBE_VehPos_Backseats"}
		};
	};
	class B_T_MBT_01_cannon_F: B_MBT_01_cannon_F{};
	class B_MBT_01_TUSK_F: B_MBT_01_cannon_F{};	//M2A1 - SlammerUP
	class B_T_MBT_01_TUSK_F: B_MBT_01_cannon_F{};
	
	
	class O_MBT_02_cannon_F	//T-100 Varsuk
	{
		positions[] =
		{
			{-1.72998,0.720703,-0.843946}, 	//Driver from Left
			{1.7583,0.720947,-0.844023},		//Driver from Right
			{-1.72998,-1.29468,-0.869474},	//Gunner
			{1.7583,-1.21387,-0.941681}		//Commander
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},	"STR_OBE_VehPos_Commander"}
		};
	};
	class O_T_MBT_02_cannon_ghex_F: O_MBT_02_cannon_F{};
	
	
	class O_MBT_02_arty_F	//T-100 Varsuk
	{
		positions[] =
		{
			{-1.72998,0.257813,-1.44617}, 	//Driver from Left
			{1.72998,0.257813,-1.44617},	//Driver from Right
			{-1.71729,-3.0127,-0.859142},	//Gunner
			{1.71729,-3.0127,-0.859142}		//Commander
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},	"STR_OBE_VehPos_Commander"}
		};
	};
	class O_T_MBT_02_arty_ghex_F: O_MBT_02_arty_F{};
	
	
	
	
	class I_MBT_03_cannon_F	//MBT-52 Kuma
	{
		positions[] =
		{
			{2.19092,0.444336,-1.1096},		//Driver
			{-2.16602,-3.05542,-1.10125},	//Gunner
			{2.17188,-3.05737,-1.10439}		//Commander
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",		{0},	"STR_OBE_VehPos_Gunner"},
			{"trt",		"getincommander_ca",	{0,0},	"STR_OBE_VehPos_Commander"}
		};
	};
	
	
	class C_Scooter_Transport_01_F	//Water Scooter
	{
		positions[] =
		{
			{0.000000,0.630859,-0.483154},		//Driver
			{0.000000,-0.0371094,-0.625061},	//Backseat
			{0.000000,-0.51123,-0.585754}		//Backseat (Rear)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Backseat"},
			{"crg",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Backseat_Rear"}
		};
	};
	
	
	class C_Boat_Civil_01_F	//Motorboat
	{
		positions[] =
		{
			{-0.964355,-0.418457,-0.375977},	//Driver from Left
			{0.951172,-0.389648,-0.388},		//Driver from Right
			{0.970215,-1.91895,-0.454895},		//Backseat (Left)
			{-0.983398,-1.91992,-0.451477}		//Backseat (Right)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Backseat_Right"},
			{"crg",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Backseat_Left"}
		};
	};
	class C_Boat_Civil_01_police_F: C_Boat_Civil_01_F{};
	class C_Boat_Civil_01_rescue_F: C_Boat_Civil_01_F{};
	
	
	class B_G_Boat_Transport_01_F	//Schlauchboot
	{
		positions[] =
		{
			{-0.814453,-1.43164,-0.628479},		//Driver from Left
			{0.827148,-1.4541,-0.631958},		//Driver from Right
			{0.839844,-0.015625,-0.633911},		//Seat (Prone Pos) (Right)
			{-0.829102,-0.0131836,-0.631409},	//Seat (Prone Pos) (Left)
			{-0.595215,1.73242,-0.605042},		//Seat (Front) from Left
			{0.601563,1.74609,-0.61792},		//Seat (Front) from Right
			{-0.845215,0.925781,-0.634216},		//Seat (Center) from Left
			{0.743652,0.919922,-0.62146}		//Seat (Center) from Right
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Seat_Right"},
			{"trt",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Seat_Left"},
			{"trt",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Seat_Front"},
			{"trt",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Seat_Front"},
			{"trt",		"getincargo_ca",	{3},	"STR_OBE_VehPos_Seat_Center"},
			{"trt",		"getincargo_ca",	{3},	"STR_OBE_VehPos_Seat_Center"}
		};
	};
	class B_Boat_Transport_01_F: B_G_Boat_Transport_01_F{};
	class B_Lifeboat: B_G_Boat_Transport_01_F{};
	class B_T_Boat_Transport_01_F: B_G_Boat_Transport_01_F{};
	class B_T_Lifeboat: B_G_Boat_Transport_01_F{};
	class C_Rubberboat: B_G_Boat_Transport_01_F{};
	class O_T_Lifeboat: B_G_Boat_Transport_01_F{};
	class O_T_Boat_Transport_01_F: B_G_Boat_Transport_01_F{};
	class O_Lifeboat: B_G_Boat_Transport_01_F{};
	class O_Boat_Transport_01_F: B_G_Boat_Transport_01_F{};
	class O_G_Boat_Transport_01_F: B_G_Boat_Transport_01_F{};
	class I_G_Boat_Transport_01_F: B_G_Boat_Transport_01_F{};
	class I_Boat_Transport_01_F: B_G_Boat_Transport_01_F{};
	class I_C_Boat_Transport_01_F: B_G_Boat_Transport_01_F{};
	
	
	class I_C_Boat_Transport_02_F	//RHIB
	{
		positions[] =
		{
			{-1.30127,-0.97998,-0.273376},	//Driver from Left
			{1.28564,-0.984863,-0.25592},	//Driver from Right
			{0.958984,2.11377,-0.0369263},	//Seat (Bow-Right)
			{-1.16211,1.521,-0.136658},		//Seat (Bow-Left)
			{-1.2915,0.400879,-0.218628},	//Seat (Center-Left)
			{-1.26807,-2.13184,-0.273376},	//Backseat (Rear-Left)
			{1.26855,-2.11523,-0.273438},	//Backseat (Rear-Right)
			{1.27441,0.0419922,-0.210815},	//Seat (Center-Right)
			{1.21484,1.0708,-0.153198}		//Seat (Bow-Right)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Seat_Front"},
			{"trt",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Seat_Front"},
			{"trt",		"getincargo_ca",	{2},	"STR_OBE_VehPos_Seat_Center"},
			{"trt",		"getincargo_ca",	{3},	"STR_OBE_VehPos_Seat_Rear"},
			{"trt",		"getincargo_ca",	{4},	"STR_OBE_VehPos_Seat_Rear"},
			{"trt",		"getincargo_ca",	{5},	"STR_OBE_VehPos_Seat_Center"},
			{"trt",		"getincargo_ca",	{6},	"STR_OBE_VehPos_Seat_Front"}
		};
	};
	class C_Boat_Transport_02_F: I_C_Boat_Transport_02_F{};
	
	
	class B_Boat_Armed_01_minigun_F	//RHIB
	{
		positions[] =
		{
			{-1.68701,0.765625,-1.57328},	//Driver from Left
			{1.70459,0.841309,-1.55573},	//Driver from Right
			{-1.67139,-1.04102,-1.60436},	//Commander from Left
			{1.70264,-0.893555,-1.60056},	//Commander from Right
			{-1.47998,-5.07373,-2.07983},	//Rear Gunner from Left
			{0.0214844,-5.25781,-2.28784},	//Rear Gunner from Center
			{1.53467,-5.03223,-2.02973},	//Rear Gunner from Right
			{1.68896,-1.75781,-1.60764},	//0 x
			{-1.67969,-1.99463,-1.63936},	//1 x
			{-1.66943,-0.249023,-1.58083},	//2 x
			{1.68213,0.0283203,-1.55173},	//3 x
			{1.72021,3.04639,-1.5472},		//4 x
			{-1.70898,3.00488,-1.5603},		//5 x
			{-1.72461,1.91064,-1.5862},		//6 x
			{1.73877,1.7085,-1.57192}		//7 x
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"drv",		"getindriver_ca",		{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getincommander_ca",	{0},	"STR_OBE_VehPos_Commander"},
			{"trt",		"getincommander_ca",	{0},	"STR_OBE_VehPos_Commander"},
			{"trt",		"getindriver_ca",		{1},	"STR_OBE_VehPos_Gunner_Rear"},
			{"trt",		"getindriver_ca",		{1},	"STR_OBE_VehPos_Gunner_Rear"},
			{"trt",		"getindriver_ca",		{1},	"STR_OBE_VehPos_Gunner_Rear"},
			{"crg",		"getincargo_ca",		{0},	"STR_OBE_VehPos_Backseat_Rear"},
			{"crg",		"getincargo_ca",		{1},	"STR_OBE_VehPos_Backseat_Rear"},
			{"crg",		"getincargo_ca",		{2},	"STR_OBE_VehPos_Backseat_Center"},
			{"crg",		"getincargo_ca",		{3},	"STR_OBE_VehPos_Backseat_Center"},
			{"crg",		"getincargo_ca",		{4},	"STR_OBE_VehPos_Backseat_Front"},
			{"crg",		"getincargo_ca",		{5},	"STR_OBE_VehPos_Backseat_Front"},
			{"crg",		"getincargo_ca",		{6},	"STR_OBE_VehPos_Backseat_Center"},
			{"crg",		"getincargo_ca",		{7},	"STR_OBE_VehPos_Backseat_Center"}
		};
	};
	class B_T_Boat_Armed_01_minigun_F: B_Boat_Armed_01_minigun_F{};
	class O_Boat_Armed_01_hmg_F: B_Boat_Armed_01_minigun_F{};
	class O_T_Boat_Armed_01_hmg_F: B_Boat_Armed_01_minigun_F{};
	class I_Boat_Armed_01_minigun_F: B_Boat_Armed_01_minigun_F{};
	
	
	class B_SDV_01_F	//Motorboat
	{
		positions[] =
		{
			{-0.70166,0.79834,-0.856995},		//Driver
			{0.607422,0.761719,-0.856995},		//Gunner
			{0.682129,-2.11328,-0.856995},		//Backseat (Right)
			{-0.694336,-2.0708,-0.856995}		//Backseat (Left)
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},		"STR_OBE_VehPos_Driver"},
			{"trt",		"getingunner_ca",	{0},	"STR_OBE_VehPos_Gunner"},
			{"crg",		"getincargo_ca",	{0},	"STR_OBE_VehPos_Backseat_Right"},
			{"crg",		"getincargo_ca",	{1},	"STR_OBE_VehPos_Backseat_Left"}
		};
	};
	class O_SDV_01_F: B_SDV_01_F{};
	class I_SDV_01_F: B_SDV_01_F{};
	
	
	class B_Plane_CAS_01_dynamicLoadout_F	//A-164 Wipeout (CAS)
	{
		positions[] =
		{
			{-0.660645,5.10034,-0.23423}		//Pilot
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",	{},	"STR_OBE_VehPos_Pilot"}
		};
	};
	
	
	class B_Plane_Fighter_01_F	//A-164 Wipeout (CAS)
	{
		positions[] =
		{
			{-0.523926,4.50366,-0.448174}		//Pilot
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",	{},	"STR_OBE_VehPos_Pilot"}
		};
	};
	class B_Plane_Fighter_01_Stealth_F: B_Plane_Fighter_01_F{};
	
	
	class B_T_VTOL_01_infantry_F	//V-44 X Blackfish (Infantry)
	{
		positions[] =
		{
			{-2.14746,7.72119,-5.33985},		//Pilot
			{-2.21289,6.52173,-5.33985},		//CoPilot
			{-2.15625,7.72656,-4.61561},		//Seat (Cockpit-Left)
			{-2.23389,6.52808,-4.61561},		//Seat (Cockpit-Right)
			{-1.4585,-5.44531,-5.13688},		//Baygunner (Left)
			{1.4585,-5.44531,-5.13688},			//Baygunner (Right)
			{-0.55217,-5.44531,-5.13688},		//Seats (Left Row)
			{0.55217,-5.44531,-5.13688}			//Seats (Right Row)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},												"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},											"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getinpilot_ca",		{1},											"STR_OBE_VehPos_Seat_Cockpit_Left"},
			{"trt",	"getinpilot_ca",		{2},											"STR_OBE_VehPos_Seat_Cockpit_Right"},
			{"trt",	"getincargo_ca",		{3},											"STR_OBE_VehPos_Gunner_Rear_Left"},
			{"trt",	"getincargo_ca",		{4},											"STR_OBE_VehPos_Gunner_Rear_Right"},
			{"crg",	"getincargo_ca",		{6,1,5,4,3,2,9,28,0,24,25,26,27,19,29},			"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{14,13,12,10,15,8,7,22,16,17,18,11,20,21,23},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	
	class B_T_VTOL_01_armed_F	//V-44 X Blackfish (Armed)
	{
		positions[] =
		{
			{-2.14746,7.72119,-5.33985},		//Pilot
			{-2.21289,6.52173,-5.33985},		//CoPilot
			{-2.15625,7.72656,-4.61561},		//Gunner (Left)
			{-2.23389,6.52808,-4.61561}		//Gunner (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getingunner_ca",		{1},	"STR_OBE_VehPos_Gunner_Left"},
			{"trt",	"getingunner_ca",		{2},	"STR_OBE_VehPos_Gunner_Right"}
		};
	};
	
	class B_T_VTOL_01_vehicle_F	//V-44 X Blackfish (Armed)
	{
		positions[] =
		{
			{-2.14746,7.72119,-5.33985},		//Pilot
			{-2.21289,6.52173,-5.33985},		//CoPilot
			{-2.15625,7.72656,-4.61561},		//Gunner (Left)
			{-2.23389,6.52808,-4.61561}		//Gunner (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getinpilot_ca",		{1},	"STR_OBE_VehPos_Seat_Cockpit_Left"},
			{"trt",	"getinpilot_ca",		{2},	"STR_OBE_VehPos_Seat_Cockpit_Right"}
		};
	};
	
	
	class O_Plane_CAS_02_dynamicLoadout_F	//To-199 Neophron (CAS)
	{
		positions[] =
		{
			{-0.829102,2.97949,-0.98571}		//Pilot
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",	{},	"STR_OBE_VehPos_Pilot"}
		};
	};
	
	
	class O_Plane_Fighter_02_F	//To-201 Shikra 
	{
		positions[] =
		{
			{-0.930664,5.70752,-0.337639}		//Pilot
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",	{},	"STR_OBE_VehPos_Pilot"}
		};
	};
	class O_Plane_Fighter_02_Stealth_F: O_Plane_Fighter_02_F{};
	
	
	class O_T_VTOL_02_infantry_dynamicLoadout_F	//Y-32 Xi'an (Infantry Transport)
	{
		positions[] =
		{
			{0.915039,4.30957,-0.986679},		//Pilot
			{-0.676758,5.46094,-1.52037},		//Gunner
			{-1.07324,-4.0000,-1.63014},		//Baygunner (Left)
			{1.07324,-4.0000,-1.63014},			//Baygunner (Right)
			{-1.41162,2.63574,-1.27126},		//Seats (Center) from Left
			{1.41162,2.63574,-1.27126},			//Seats (Center) from Right
			{-0.448242,-4.0000,-1.63014},		//Seats (Left Row)
			{0.448242,-4.0000,-1.63014}			//Seats (Right Row)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},				"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},			"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getingunner_ca",		{1},			"STR_OBE_VehPos_Gunner_Rear_Left"},
			{"trt",	"getingunner_ca",		{2},			"STR_OBE_VehPos_Gunner_Rear_Right"},
			{"crg",	"getincargo_ca",		{13,12,11,0},	"STR_OBE_VehPos_Backseats_Center"},
			{"crg",	"getincargo_ca",		{0,1,2,13},		"STR_OBE_VehPos_Backseats_Center"},
			{"crg",	"getincargo_ca",		{7,8,9,10,11},	"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{6,5,4,3,2},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	
	
	class O_T_VTOL_02_vehicle_dynamicLoadout_F	//Y-32 Xi'an (Vehicle Transport)
	{
		positions[] =
		{
			{0.856445,4.43311,-0.972366},		//Pilot
			{-0.676758,5.46094,-1.52037}		//Gunner
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"}
		};
	};
	
	
	class I_Plane_Fighter_03_dynamicLoadout_F	//A-143 Buzzard (CAS) 
	{
		positions[] =
		{
			{-0.57959,2.61377,-0.743408}		//Pilot
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",	{},	"STR_OBE_VehPos_Pilot"}
		};
	};
	
	
	class I_Plane_Fighter_04_F	//A-149 Gryphon
	{
		positions[] =
		{
			{0.799805,4.05127,-0.63266}		//Pilot
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",	{},	"STR_OBE_VehPos_Pilot"}
		};
	};
	
	class C_Plane_Civil_01_F	//Caesar BTT
	{
		positions[] =
		{
			{-0.68457,1.26123,-0.183607},		//Pilot
			{0.672852,1.36475,-0.163094},		//CoPilot
			{0.672852,0.202148,-0.203547},		//Seats (Right)
			{-0.68457,0.21582,-0.286988}		//Seats (Left)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"crg",	"getincargo_ca",		{0},	"STR_OBE_VehPos_Seat_Right"},
			{"crg",	"getincargo_ca",		{1},	"STR_OBE_VehPos_Seat_Left"}
		};
	};
	class C_Plane_Civil_01_racing_F: C_Plane_Civil_01_F{};
	class I_C_Plane_Civil_01_F: C_Plane_Civil_01_F{};
	
	
	class C_Heli_Light_01_civil_F	//M-900
	{
		positions[] =
		{
			{-0.620605,2.6792,0.763738},		//Pilot
			{0.620605,2.6792,0.763738},			//CoPilot
			{0.571289,0.810547,0.763738},		//Seat (Right)
			{-0.571289,0.810547,0.763738}		//Seat (Left)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"crg",	"getincargo_ca",		{0},	"STR_OBE_VehPos_Seat_Right"},
			{"crg",	"getincargo_ca",		{1},	"STR_OBE_VehPos_Seat_Left"}
		};
	};
	
	class I_C_Heli_Light_01_civil_F
	{
		positions[] =
		{
			{-0.621094,1.82422,-0.433683},		//Pilot
			{0.621094,1.82422,-0.433683},		//CoPilot
			{0.621094,0.0224609,-0.433683},		//Seats (Right)
			{-0.621094,0.0224609,-0.433683}		//Seats (Left)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"crg",	"getincargo_ca",		{0},	"STR_OBE_VehPos_Seat_Right"},
			{"crg",	"getincargo_ca",		{1},	"STR_OBE_VehPos_Seat_Left"}
		};
	};
	
	
	
	class B_Heli_Light_01_F	//MH-9 Hummingbird
	{
		positions[] =
		{
			{-0.621094,1.82422,-0.433683},		//Pilot
			{0.621094,1.82422,-0.433683},		//CoPilot
			{-0.57959,-0.0737305,-0.404733},	//Seat (Right)			0
			{0.57959,-0.0737305,-0.404733},		//Seat (Left)			1
			{1.00000,0.944824,-0.848734},		//Bench (Front) Right	2
			{-1.00000,0.0678711,-0.848734},		//Bench (Rear) Left		3
			{-1.00000,0.944824,-0.848734},		//Bench (Front) Left	4
			{1.00000,0.0678711,-0.848734}		//Bench (Rear) Right	5
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"crg",	"getincargo_ca",		{0},	"STR_OBE_VehPos_Seat_Left"},
			{"crg",	"getincargo_ca",		{1},	"STR_OBE_VehPos_Seat_Right"},
			{"trt",	"getincargo_ca",		{1},	"STR_OBE_VehPos_Seat_Front"},
			{"trt",	"getincargo_ca",		{2},	"STR_OBE_VehPos_Seat_Rear"},
			{"trt",	"getincargo_ca",		{3},	"STR_OBE_VehPos_Seat_Front"},
			{"trt",	"getincargo_ca",		{4},	"STR_OBE_VehPos_Seat_Rear"}
		};
	};
	
	
	class B_Heli_Light_01_dynamicLoadout_F	//AH-9 Pawnee
	{
		positions[] =
		{
			{-0.621094,1.82422,-0.433683},		//Pilot
			{0.621094,1.82422,-0.433683}		//CoPilot
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"}
		};
	};
	
	
	
	class B_Heli_Transport_01_F	//UH-80 Ghosthawk
	{
		positions[] =
		{
			{-1.26123,5.22217,-0.83588},	//Pilot
			{1.26123,5.22217,-0.83588},		//CoPilot
			{-1.3208,3.79053,-0.888888},	//Gunner (Left)
			{1.3208,3.79053,-0.888888},		//Gunner (Right)
			{-1.32471,2.42188,-0.857474},	//Backseats (Center-Left)
			{1.32471,2.42188,-0.857474},	//Backseats (Center-Right)
			{-1.32813,1.32373,-0.85305},	//Backseats (Back-Left)
			{1.32813,1.32373,-0.85305}		//Backseats (Back-Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getingunner_ca",		{1},	"STR_OBE_VehPos_Gunner_Left"},
			{"trt",	"getingunner_ca",		{2},	"STR_OBE_VehPos_Gunner_Right"},
			{"crg",	"getincargo_ca",		{3,7},	"STR_OBE_VehPos_Backseat_Center"},
			{"crg",	"getincargo_ca",		{1,6},	"STR_OBE_VehPos_Backseat_Center"},
			{"crg",	"getincargo_ca",		{2,5},	"STR_OBE_VehPos_Backseat_Left"},
			{"crg",	"getincargo_ca",		{0,4},	"STR_OBE_VehPos_Backseat_Right"}
		};
	};
	class B_CTRG_Heli_Transport_01_sand_F: B_Heli_Transport_01_F{};
	class B_CTRG_Heli_Transport_01_tropic_F: B_Heli_Transport_01_F{};
	
	
	class B_Heli_Transport_03_F		//CH-67 Huron Armed
	{
		positions[] =
		{
			{1.65381,5.22705,-1.66064},			//Pilot
			{-1.65381,5.22705,-1.66064},		//CoPilot
			{-1.70508,4.1123,-1.66064},			//Doorgunner (Left)
			{1.70508,4.1123,-1.66064},			//Doorgunner (Right)
			{1.13867,-5.40576,-1.61612},		//Baygunner (Right)
			{-1.13867,-5.40576,-1.61612},		//Baygunner (Left)
			{-0.407227,-5.40576,-1.61612},		//Backseats (Left)
			{0.407227,-5.40576,-1.61612}		//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},						"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},					"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getingunner_ca",		{1},					"STR_OBE_VehPos_Gunner_Door"},
			{"trt",	"getingunner_ca",		{2},					"STR_OBE_VehPos_Gunner_Door"},
			{"trt",	"getincargo_ca",		{3},					"STR_OBE_VehPos_Gunner_Rear_Right"},
			{"trt",	"getincargo_ca",		{4},					"STR_OBE_VehPos_Gunner_Rear_Left"},
			{"crg",	"getincargo_ca",		{1,9,2,3,4,5,0},		"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{6,7,8,12,10,11,13},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	
	
	class B_Heli_Transport_03_unarmed_F		//CH-67 Huron Unarmed
	{
		positions[] =
		{
			{1.65381,5.22705,-1.66064},			//Pilot
			{-1.65381,5.22705,-1.66064},		//CoPilot
			{-1.70508,4.1123,-1.66064},			//Windowgunner (Left)
			{1.70508,4.1123,-1.66064},			//Windowgunner (Right)
			{1.13867,-5.40576,-1.61612},		//Baygunner (Right)
			{-1.13867,-5.40576,-1.61612},		//Baygunner (Left)
			{-0.407227,-5.40576,-1.61612},		//Backseats (Left)
			{0.407227,-5.40576,-1.61612}		//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},						"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},					"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getingunner_ca",		{1},					"STR_OBE_VehPos_Gunner_Window"},
			{"trt",	"getingunner_ca",		{2},					"STR_OBE_VehPos_Gunner_Window"},
			{"trt",	"getincargo_ca",		{3},					"STR_OBE_VehPos_Gunner_Rear_Right"},
			{"trt",	"getincargo_ca",		{4},					"STR_OBE_VehPos_Gunner_Rear_Left"},
			{"crg",	"getincargo_ca",		{1,9,2,3,4,5,0},		"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{6,7,8,12,10,11,13},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	
	
	class B_UGV_01_F	//UGV Saif
	{
		positions[] =
		{
			{-0.355225,0.785156,-0.69545}			//Passenger
		};
		position_infos[] =
		{
			{"trt",	"getingunner_ca",		{1},					"STR_OBE_VehPos_Backseat"}
		};
	};
	class O_UGV_01_F: B_UGV_01_F{};
	class I_UGV_01_F: B_UGV_01_F{};
	class O_T_UGV_01_ghex_F: B_UGV_01_F{};
	//UGV Saif RCWS (Armed)
	class B_UGV_01_rcws_F: B_UGV_01_F{};
	class O_UGV_01_rcws_F: B_UGV_01_F{};
	class I_UGV_01_rcws_F: B_UGV_01_F{};
	class O_T_UGV_01_rcws_ghex_F: B_UGV_01_F{};
	
	
	class I_Heli_Transport_02_F		//CH-49 Mohawk
	{
		positions[] =
		{
			{-1.46924,5.12842,-2.14212},		//Pilot
			{-1.46924,5.12842,-1.63713},		//CoPilot
			{-0.93457,-3.51172,-1.95628},		//Baygunner (Left)
			{0.93457,-3.51172,-1.95628},		//Baygunner (Right)
			{-0.341797,-3.51758,-1.95628},		//Backseats Rear (Left)
			{0.341797,-3.51758,-1.95628},		//Backseats Rear (Right)
			{-1.46924,4.15479,-2.14212},		//Backseats Center
			{1.59766,2.27295,-1.98402}			//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},					"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},				"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getincargo_ca",		{1},				"STR_OBE_VehPos_Gunner_Rear_Left"},
			{"trt",	"getincargo_ca",		{2},				"STR_OBE_VehPos_Gunner_Rear_Right"},
			{"crg",	"getincargo_ca",		{0,11,12,5,10,2},	"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{6,3,1,4,8,9},		"STR_OBE_VehPos_Backseats_Right"},
			{"crg",	"getincargo_ca",		{7,8,1,6,13},		"STR_OBE_VehPos_Backseats_Center"},
			{"crg",	"getincargo_ca",		{3,6,1,4,8,9},		"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	
	class O_Heli_Light_02_unarmed_F		//PO-30 Orca (Unarmed)
	{
		positions[] =
		{
			{0.900879,3.7251,-1.07571},			//Pilot
			{-0.905273,3.7251,-1.07571},		//CoPilot
			{-1.06543,1.4209,-1.05698},		//Backseats (Left)
			{1.05664,1.41797,-1.06995}		//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},					"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},				"STR_OBE_VehPos_CoPilot"},
			{"crg",	"getincargo_ca",		{4,5,7,2,1},		"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{4,3,6,0,1},		"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	class O_Heli_Light_02_dynamicLoadout_F: O_Heli_Light_02_unarmed_F{};
	
	
	class B_Heli_Attack_01_dynamicLoadout_F	//AH-99 Blackfoot
	{
		positions[] =
		{
			{-0.728516,4.00488,-0.683043},		//Pilot
			{-0.91748,2.69482,-0.449699}		//CoPilot
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"}
		};
	};
	
	
	class O_Heli_Attack_02_dynamicLoadout_F	//Mi-48 Kajman
	{
		positions[] =
		{
			{0.955566,2.53711,-1.73589},		//Pilot from Right
			{0.955566,2.53711,-1.25527},		//CoPilot from Right
			{-0.955566,2.53711,-1.73589},		//Pilot from Left
			{-0.955566,2.53711,-1.25527},		//CoPilot from Left
			{-0.955566,1.75195,-1.77869},		//Backseats (Left)
			{0.955566,1.75195,-1.77869}			//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},				"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},			"STR_OBE_VehPos_CoPilot"},
			{"drv",	"getinpilot_ca",		{},				"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},			"STR_OBE_VehPos_CoPilot"},
			{"crg",	"getincargo_ca",		{3,2,1,0},		"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{7,6,4,5},		"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	
	class I_Heli_light_03_unarmed_F	//WY-55 Hellcat (Unarmed)
	{
		positions[] =
		{
			{0.919434,3.42041,-0.37443},		//Pilot
			{-0.919434,3.42041,-0.37443},		//CoPilot
			{-0.910645,2.23438,-0.585531},		//Backseats (Left)
			{0.914551,2.32422,-0.49726}			//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},				"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},			"STR_OBE_VehPos_CoPilot"},
			{"crg",	"getincargo_ca",		{2,5,1},		"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{3,4,0},		"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	
	class I_Heli_light_03_dynamicLoadout_F	//WY-55 Hellcat (Armed)
	{
		positions[] =
		{
			{0.919434,3.42041,-0.37443},	//Pilot
			{-0.919434,3.42041,-0.37443},	//CoPilot
			{0.942383,2.10449,-0.785331},	//Doorgunner (Right)
			{-0.942383,2.10449,-0.785331},	//Doorgunner (Left)
			{-0.927246,1.3877,-0.437634},	//Backseats (Left)
			{0.927246,1.3877,-0.437634}		//Backseats (Right)
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},				"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},			"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getincargo_ca",		{1},			"STR_OBE_VehPos_Gunner_Door"},
			{"trt",	"getincargo_ca",		{2},			"STR_OBE_VehPos_Gunner_Door"},
			{"crg",	"getincargo_ca",		{0,1,2,3},		"STR_OBE_VehPos_Backseats"},
			{"crg",	"getincargo_ca",		{3,2,1,0},		"STR_OBE_VehPos_Backseats"}
		};
	};
	
	
	
	class O_Heli_Transport_04_F	//Mi-290 Taru +SubClasses (Ammo, Fuel, etc)
	{
		positions[] =
		{
			{-1.23096,4.0415,-1.06958},		//Pilot
			{1.23096,4.0415,-1.06958},		//CoPilot
			{1.32764,3.12402,-1.39239}	//Loadmaster
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},				"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},			"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getincargo_ca",		{1},			"STR_OBE_VehPos_Loadmaster"}
		};
	};
	class O_Heli_Transport_04_ammo_F: O_Heli_Transport_04_F{};
	class O_Heli_Transport_04_box_F: O_Heli_Transport_04_F{};
	class O_Heli_Transport_04_fuel_F: O_Heli_Transport_04_F{};
	class O_Heli_Transport_04_repair_F: O_Heli_Transport_04_F{};
	
	
	
	class O_Heli_Transport_04_bench_F	//Mi-290 Taru (Bench)
	{
		positions[] =
		{
			{-1.23047,3.979,-1.07482},			//Pilot
			{1.23047,3.979,-1.07482},			//CoPilot
			{1.32764,3.12402,-1.39239},			//Loadmaster
			{0.382324,1.39648,-2.11482},		//Seat 2
			{0.382324,0.559082,-2.11482},		//Seat 3
			{0.382324,-0.270996,-2.11482},		//Seat 4
			{0.382324,-1.10205,-2.11482},		//Seat 5
			{-0.382324,-1.10205,-2.11482},		//Seat 6
			{-0.382324,-0.270996,-2.11482},		//Seat 7
			{-0.382324,0.559082,-2.11482},		//Seat 8
			{-0.382324,1.39648,-2.11482}		//Seat 9
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getincargo_ca",		{1},	"STR_OBE_VehPos_Loadmaster"},
			{"trt",	"getincargo_ca",		{2},	"STR_OBE_VehPos_Seat"},
			{"trt",	"getincargo_ca",		{3},	"STR_OBE_VehPos_Seat"},
			{"trt",	"getincargo_ca",		{4},	"STR_OBE_VehPos_Seat"},
			{"trt",	"getincargo_ca",		{5},	"STR_OBE_VehPos_Seat"},
			{"trt",	"getincargo_ca",		{6},	"STR_OBE_VehPos_Seat"},
			{"trt",	"getincargo_ca",		{7},	"STR_OBE_VehPos_Seat"},
			{"trt",	"getincargo_ca",		{8},	"STR_OBE_VehPos_Seat"},
			{"trt",	"getincargo_ca",		{9},	"STR_OBE_VehPos_Seat"}
		};
	};
	
	
	
	class O_Heli_Transport_04_covered_F	//Mi-290 Taru (Transport)
	{
		positions[] =
		{
			{-1.23047,3.979,-1.07482},			//Pilot
			{1.23047,3.979,-1.07482},			//CoPilot
			{1.32764,3.12402,-1.39239},			//Loadmaster
			{0.76416,-3.65576,-1.05032},		//Baygunner (Right
			{-0.76416,-3.65576,-1.05032},		//Baygunner (Left)
			{-1.41895,1.39404,-1.16059},		//Bench (Left) from Left
			{-0.243164,-3.66797,-1.05404},		//Bench (Left) from Rear
			{0.243164,-3.66797,-1.05404},		//Bench (Right) from Rear
			{1.41895,1.39404,-1.16059}			//Bench (Right) from Right
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},					"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},				"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getincargo_ca",		{1},				"STR_OBE_VehPos_Loadmaster"},
			{"trt",	"getincargo_ca",		{2},				"STR_OBE_VehPos_Gunner_Rear_Right"},
			{"trt",	"getincargo_ca",		{3},				"STR_OBE_VehPos_Gunner_Rear_Left"},
			{"crg",	"getincargo_ca",		{1,3,5,7,9,10,11},	"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{11,10,9,7,5,3,1},	"STR_OBE_VehPos_Backseats_Left"},
			{"crg",	"getincargo_ca",		{13,12,8,6,4,2,0},	"STR_OBE_VehPos_Backseats_Right"},
			{"crg",	"getincargo_ca",		{0,2,4,6,8,12,13},	"STR_OBE_VehPos_Backseats_Right"}
		};
	};
	
	
	class O_Heli_Transport_04_medevac_F	//Mi-290 Taru (Medical)
	{
		positions[] =
		{
			{-1.23047,3.979,-1.07482},			//Pilot
			{1.23047,3.979,-1.07482},			//CoPilot
			{1.32764,3.12402,-1.07482},			//Loadmaster
			{-0.42627,-3.64111,-1.08613},		//Flatbed (Rear-Left)
			{0.42627,-3.64111,-1.08613},		//Flatbed (Rear-Right)
			{-1.42041,1.90039,-1.17076},		//Flatbed (Front-Right)
			{-1.42041,1.1167,-1.17076}			//Seat
		};
		position_infos[] =
		{
			{"drv",	"getinpilot_ca",		{},		"STR_OBE_VehPos_Pilot"},
			{"trt",	"getincommander_ca",	{0},	"STR_OBE_VehPos_CoPilot"},
			{"trt",	"getincargo_ca",		{1},	"STR_OBE_VehPos_Loadmaster"},
			{"crg",	"getincargo_ca",		{0},	"STR_OBE_VehPos_Flatbed_Left"},
			{"crg",	"getincargo_ca",		{1},	"STR_OBE_VehPos_Flatbed_Right"},
			{"crg",	"getincargo_ca",		{2},	"STR_OBE_VehPos_Flatbed_Front"},
			{"crg",	"getincargo_ca",		{3},	"STR_OBE_VehPos_Seat_Left"}
		};
	};
	
	
};

/*
Icons

Driver		=	getindriver_ca
Pilot		=	getinpilot_ca
Gunner		=	getingunner_ca
Commander	=	getincommander_ca
Cargo		=	getincargo_ca

Other available IconsPath:
a3\ui_f\data\IGUI\Cfg\Actions\NameOfDesiredIcon.paa
( e.g. getincargo_ca -> "a3\ui_f\data\IGUI\Cfg\Actions\getincargo_ca.paa" )


Stringtable Available Names:

//Mainseats
	"STR_OBE_VehPos_Driver"
	"STR_OBE_VehPos_Pilot"
	"STR_OBE_VehPos_CoDriver"
	"STR_OBE_VehPos_CoPilot"
	"STR_OBE_VehPos_Commander"
	"STR_OBE_VehPos_Loadmaster"
	
//Gunner Pos
	"STR_OBE_VehPos_Gunner"
	"STR_OBE_VehPos_Gunner_Right"
	"STR_OBE_VehPos_Gunner_Center"
	"STR_OBE_VehPos_Gunner_Left"
	"STR_OBE_VehPos_Gunner_Door"
	"STR_OBE_VehPos_Gunner_Window"
	"STR_OBE_VehPos_Gunner_Rear"
	"STR_OBE_VehPos_Gunner_Rear_Right"
	"STR_OBE_VehPos_Gunner_Rear_Left"
	
//Backseat - Singular	
	"STR_OBE_VehPos_Backseat"
	"STR_OBE_VehPos_Backseat_Right"
	"STR_OBE_VehPos_Backseat_Left"
	"STR_OBE_VehPos_Backseat_Front"
	"STR_OBE_VehPos_Backseat_Center"
	"STR_OBE_VehPos_Backseat_Rear"
	
//Backseat - Plural
	"STR_OBE_VehPos_Backseats"
	"STR_OBE_VehPos_Backseats_Right"
	"STR_OBE_VehPos_Backseats_Left"
	"STR_OBE_VehPos_Backseats_Front"
	"STR_OBE_VehPos_Backseats_Center"
	"STR_OBE_VehPos_Backseats_Rear"
	
//Seat - Singular
	"STR_OBE_VehPos_Seat"
	"STR_OBE_VehPos_Seat_Cockpit_Right"
	"STR_OBE_VehPos_Seat_Cockpit_Left"
	"STR_OBE_VehPos_Seat_Right"
	"STR_OBE_VehPos_Seat_Left"
	"STR_OBE_VehPos_Seat_Front"
	"STR_OBE_VehPos_Seat_Center"
	"STR_OBE_VehPos_Seat_Rear"
	
//Seat - Plural
	"STR_OBE_VehPos_Seats"
	"STR_OBE_VehPos_Seats_Cockpit_Right"
	"STR_OBE_VehPos_Seats_Cockpit_Left"
	"STR_OBE_VehPos_Seats_Right"
	"STR_OBE_VehPos_Seats_Left"
	"STR_OBE_VehPos_Seats_Front"
	"STR_OBE_VehPos_Seats_Center"
	"STR_OBE_VehPos_Seats_Rear"
	
//Flatbed (e.g. in Taru-Medic)
	"STR_OBE_VehPos_Flatbed"
	"STR_OBE_VehPos_Flatbed_Right"
	"STR_OBE_VehPos_Flatbed_Left"
	"STR_OBE_VehPos_Flatbed_Front"
	"STR_OBE_VehPos_Flatbed_Center"
	"STR_OBE_VehPos_Flatbed_Rear"
*/



/////////////////////////////////////////////////////
//////////////// CONFIG CHANGES /////////////////////
/////////////////////////////////////////////////////
#define RemoveGetIn getInRadius = 0.0001;
class CfgVehicles
{
	
	class Heli_Transport_04_base_F;
	class O_Heli_Transport_04_F: Heli_Transport_04_base_F { RemoveGetIn };
	class O_Heli_Transport_04_ammo_F: Heli_Transport_04_base_F { RemoveGetIn };
	class O_Heli_Transport_04_box_F: Heli_Transport_04_base_F { RemoveGetIn };
	class O_Heli_Transport_04_fuel_F: Heli_Transport_04_base_F { RemoveGetIn };
	class O_Heli_Transport_04_repair_F: Heli_Transport_04_base_F { RemoveGetIn };
	class O_Heli_Transport_04_bench_F: Heli_Transport_04_base_F { RemoveGetIn };
	class O_Heli_Transport_04_covered_F: Heli_Transport_04_base_F { RemoveGetIn };
	class O_Heli_Transport_04_medevac_F: Heli_Transport_04_base_F { RemoveGetIn };
	
	class Heli_light_03_unarmed_base_F;
	class I_Heli_light_03_unarmed_F: Heli_light_03_unarmed_base_F { RemoveGetIn };
	class Heli_light_03_dynamicLoadout_base_F;
	class I_Heli_light_03_dynamicLoadout_F: Heli_light_03_dynamicLoadout_base_F { RemoveGetIn };

	class Heli_Attack_02_dynamicLoadout_base_F;
	class O_Heli_Attack_02_dynamicLoadout_F: Heli_Attack_02_dynamicLoadout_base_F { RemoveGetIn };
	
	class Heli_Attack_01_dynamicLoadout_base_F;
	class B_Heli_Attack_01_dynamicLoadout_F: Heli_Attack_01_dynamicLoadout_base_F { RemoveGetIn };

	class Heli_Light_02_unarmed_base_F;
	class O_Heli_Light_02_unarmed_F: Heli_Light_02_unarmed_base_F { RemoveGetIn };
	class Heli_Light_02_dynamicLoadout_base_F;
	class O_Heli_Light_02_dynamicLoadout_F: Heli_Light_02_dynamicLoadout_base_F { RemoveGetIn };
	
	class Heli_Transport_02_base_F;
	class I_Heli_Transport_02_F: Heli_Transport_02_base_F { RemoveGetIn };
	
	class UGV_01_rcws_base_F;
	class B_UGV_01_rcws_F: UGV_01_rcws_base_F { RemoveGetIn };
	class O_UGV_01_rcws_F: UGV_01_rcws_base_F { RemoveGetIn };
	class I_UGV_01_rcws_F: UGV_01_rcws_base_F { RemoveGetIn };
	class O_T_UGV_01_rcws_ghex_F: UGV_01_rcws_base_F { RemoveGetIn };
	
	class UGV_01_base_F;
	class B_UGV_01_F: UGV_01_base_F { RemoveGetIn };
	class O_UGV_01_F: UGV_01_base_F { RemoveGetIn };
	class I_UGV_01_F: UGV_01_base_F { RemoveGetIn };
	class O_T_UGV_01_ghex_F: UGV_01_base_F { RemoveGetIn };
	
	class Heli_Transport_03_base_F;
	class B_Heli_Transport_03_F: Heli_Transport_03_base_F { RemoveGetIn };
	
	class Heli_Transport_03_unarmed_base_F;
	class B_Heli_Transport_03_unarmed_F: Heli_Transport_03_unarmed_base_F { RemoveGetIn };

	class Heli_Transport_01_base_F;
	class B_Heli_Transport_01_F: Heli_Transport_01_base_F { RemoveGetIn };
	class B_CTRG_Heli_Transport_01_sand_F: Heli_Transport_01_base_F { RemoveGetIn };
	class B_CTRG_Heli_Transport_01_tropic_F: Heli_Transport_01_base_F { RemoveGetIn };
	
	class Heli_Light_01_unarmed_base_F;
	class B_Heli_Light_01_F: Heli_Light_01_unarmed_base_F { RemoveGetIn };
	
	class Heli_Light_01_dynamicLoadout_base_F;
	class B_Heli_Light_01_dynamicLoadout_F: Heli_Light_01_dynamicLoadout_base_F { RemoveGetIn };
	
	class Heli_Light_01_civil_base_F;
	class C_Heli_Light_01_civil_F: Heli_Light_01_civil_base_F { RemoveGetIn };
	class I_C_Heli_Light_01_civil_F: Heli_Light_01_civil_base_F { RemoveGetIn };

	class Plane_Fighter_03_dynamicLoadout_base_F;
	class I_Plane_Fighter_03_dynamicLoadout_F: Plane_Fighter_03_dynamicLoadout_base_F { RemoveGetIn };
	
	class Plane_Fighter_04_Base_F;
	class I_Plane_Fighter_04_F: Plane_Fighter_04_Base_F { RemoveGetIn };

	class Plane_Civil_01_base_F;
	class C_Plane_Civil_01_F: Plane_Civil_01_base_F { RemoveGetIn };
	
	
	class C_Plane_Civil_01_racing_F: Plane_Civil_01_base_F { RemoveGetIn };
	class I_C_Plane_Civil_01_F: Plane_Civil_01_base_F { RemoveGetIn };

	class VTOL_02_infantry_dynamicLoadout_base_F;
	class O_T_VTOL_02_infantry_dynamicLoadout_F: VTOL_02_infantry_dynamicLoadout_base_F { RemoveGetIn };
	class VTOL_02_vehicle_dynamicLoadout_base_F;
	class O_T_VTOL_02_vehicle_dynamicLoadout_F: VTOL_02_vehicle_dynamicLoadout_base_F { RemoveGetIn };

	class Plane_CAS_02_dynamicLoadout_base_F;
	class O_Plane_CAS_02_dynamicLoadout_F: Plane_CAS_02_dynamicLoadout_base_F { RemoveGetIn };
	
	class Plane_Fighter_02_Base_F;
	class O_Plane_Fighter_02_F: Plane_Fighter_02_Base_F { RemoveGetIn };
	class O_Plane_Fighter_02_Stealth_F: Plane_Fighter_02_Base_F { RemoveGetIn };

	class VTOL_01_vehicle_base_F;
	class B_T_VTOL_01_vehicle_F: VTOL_01_vehicle_base_F { RemoveGetIn };

	class VTOL_01_armed_base_F;
	class B_T_VTOL_01_armed_F: VTOL_01_armed_base_F { RemoveGetIn };

	class VTOL_01_infantry_base_F;
	class B_T_VTOL_01_infantry_F: VTOL_01_infantry_base_F { RemoveGetIn };

	class Plane_Fighter_01_Base_F;
	class B_Plane_Fighter_01_F: Plane_Fighter_01_Base_F { RemoveGetIn };
	class B_Plane_Fighter_01_Stealth_F: Plane_Fighter_01_Base_F { RemoveGetIn };

	class Plane_CAS_01_dynamicLoadout_base_F;
	class B_Plane_CAS_01_dynamicLoadout_F: Plane_CAS_01_dynamicLoadout_base_F { RemoveGetIn };

	class SDV_01_base_F;
	class B_SDV_01_F: SDV_01_base_F { RemoveGetIn };
	class O_SDV_01_F: SDV_01_base_F { RemoveGetIn };
	class I_SDV_01_F: SDV_01_base_F { RemoveGetIn };

	class Boat_Armed_01_minigun_base_F;
	class B_Boat_Armed_01_minigun_F: Boat_Armed_01_minigun_base_F { RemoveGetIn };
	class B_T_Boat_Armed_01_minigun_F: B_Boat_Armed_01_minigun_F { RemoveGetIn };
	class I_Boat_Armed_01_minigun_F: Boat_Armed_01_minigun_base_F { RemoveGetIn };

	class Boat_Armed_01_base_F;
	class O_Boat_Armed_01_hmg_F: Boat_Armed_01_base_F { RemoveGetIn };
	class O_T_Boat_Armed_01_hmg_F: O_Boat_Armed_01_hmg_F { RemoveGetIn };

	class Boat_Transport_02_base_F;
	class I_C_Boat_Transport_02_F: Boat_Transport_02_base_F { RemoveGetIn };
	class C_Boat_Transport_02_F: Boat_Transport_02_base_F { RemoveGetIn };

	class Rubber_duck_base_F;
	class I_G_Boat_Transport_01_F: Rubber_duck_base_F { RemoveGetIn };
	class O_G_Boat_Transport_01_F: I_G_Boat_Transport_01_F { RemoveGetIn };
	class B_G_Boat_Transport_01_F: I_G_Boat_Transport_01_F { RemoveGetIn };
	class I_Boat_Transport_01_F: Rubber_duck_base_F { RemoveGetIn };
	class I_C_Boat_Transport_01_F: Rubber_duck_base_F { RemoveGetIn };
	class O_Boat_Transport_01_F: Rubber_duck_base_F { RemoveGetIn };
	class O_T_Boat_Transport_01_F: Rubber_duck_base_F { RemoveGetIn };
	class B_Boat_Transport_01_F: Rubber_duck_base_F { RemoveGetIn };
	class B_T_Boat_Transport_01_F: B_Boat_Transport_01_F { RemoveGetIn };

	class Rescue_duck_base_F;
	class C_Rubberboat: Rescue_duck_base_F { RemoveGetIn };
	class B_Lifeboat: Rescue_duck_base_F { RemoveGetIn };
	class B_T_Lifeboat: B_Lifeboat { RemoveGetIn };
	class O_Lifeboat: Rescue_duck_base_F { RemoveGetIn };
	class O_T_Lifeboat: O_Lifeboat { RemoveGetIn };

	class Boat_Civil_01_base_F;
	class C_Boat_Civil_01_F: Boat_Civil_01_base_F { RemoveGetIn };
	class C_Boat_Civil_01_police_F: Boat_Civil_01_base_F { RemoveGetIn };
	class C_Boat_Civil_01_rescue_F: Boat_Civil_01_base_F { RemoveGetIn };

	class Scooter_Transport_01_base_F;
	class C_Scooter_Transport_01_F: Scooter_Transport_01_base_F { RemoveGetIn };

	class I_APC_tracked_03_base_F;
	class I_APC_tracked_03_cannon_F: I_APC_tracked_03_base_F { RemoveGetIn };

	class I_APC_Wheeled_03_base_F;
	class I_APC_Wheeled_03_cannon_F: I_APC_Wheeled_03_base_F { RemoveGetIn };

	class O_APC_Tracked_02_base_F;
	class O_APC_Tracked_02_cannon_F: O_APC_Tracked_02_base_F { RemoveGetIn };
	class O_T_APC_Tracked_02_cannon_ghex_F: O_APC_Tracked_02_cannon_F { RemoveGetIn };
	class O_APC_Tracked_02_AA_F: O_APC_Tracked_02_base_F { RemoveGetIn };
	class O_T_APC_Tracked_02_AA_ghex_F: O_APC_Tracked_02_AA_F { RemoveGetIn };

	class O_APC_Wheeled_02_base_F;
	class O_APC_Wheeled_02_rcws_F: O_APC_Wheeled_02_base_F { RemoveGetIn };
	class O_T_APC_Wheeled_02_rcws_ghex_F: O_APC_Wheeled_02_base_F { RemoveGetIn };

	class B_APC_Tracked_01_base_F;
	class B_APC_Tracked_01_rcws_F: B_APC_Tracked_01_base_F { RemoveGetIn };
	class B_T_APC_Tracked_01_rcws_F: B_APC_Tracked_01_rcws_F { RemoveGetIn };
	class B_APC_Tracked_01_CRV_F: B_APC_Tracked_01_base_F { RemoveGetIn };
	class B_T_APC_Tracked_01_CRV_F: B_APC_Tracked_01_CRV_F { RemoveGetIn };
	class B_APC_Tracked_01_AA_F: B_APC_Tracked_01_base_F { RemoveGetIn };
	class B_T_APC_Tracked_01_AA_F: B_APC_Tracked_01_AA_F { RemoveGetIn };

	class B_APC_Wheeled_01_base_F;
	class B_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_base_F { RemoveGetIn };
	class B_T_APC_Wheeled_01_cannon_F: B_APC_Wheeled_01_cannon_F { RemoveGetIn };

	class MRAP_02_hmg_base_F;
	class O_MRAP_02_hmg_F: MRAP_02_hmg_base_F { RemoveGetIn };
	class O_T_MRAP_02_hmg_ghex_F: MRAP_02_hmg_base_F { RemoveGetIn };

	class MRAP_02_gmg_base_F;
	class O_T_MRAP_02_gmg_ghex_F: MRAP_02_gmg_base_F { RemoveGetIn };
	class O_MRAP_02_gmg_F: MRAP_02_gmg_base_F { RemoveGetIn };

	class MRAP_02_base_F;
	class O_MRAP_02_F: MRAP_02_base_F { RemoveGetIn };
	class O_T_MRAP_02_ghex_F: MRAP_02_base_F { RemoveGetIn };

	class MRAP_01_base_F;
	class B_MRAP_01_F: MRAP_01_base_F { RemoveGetIn };

	class MRAP_01_hmg_base_F;
	class B_MRAP_01_hmg_F: MRAP_01_hmg_base_F { RemoveGetIn };
	class B_T_MRAP_01_hmg_F: B_MRAP_01_hmg_F { RemoveGetIn };

	class MRAP_01_gmg_base_F;
	class B_MRAP_01_gmg_F: MRAP_01_gmg_base_F { RemoveGetIn };
	class B_T_MRAP_01_gmg_F: B_MRAP_01_gmg_F { RemoveGetIn };

	class MRAP_03_base_F;
	class I_MRAP_03_F: MRAP_03_base_F { RemoveGetIn };

	class MRAP_03_gmg_base_F;
	class I_MRAP_03_gmg_F: MRAP_03_gmg_base_F { RemoveGetIn };

	class MRAP_03_hmg_base_F;
	class I_MRAP_03_hmg_F: MRAP_03_hmg_base_F { RemoveGetIn };

	class LSV_02_armed_base_F;
	class O_T_LSV_02_armed_F: LSV_02_armed_base_F { RemoveGetIn };
	class O_LSV_02_armed_F: LSV_02_armed_base_F { RemoveGetIn };

	class LSV_02_unarmed_base_F;
	class O_T_LSV_02_unarmed_F: LSV_02_unarmed_base_F { RemoveGetIn };
	class O_LSV_02_unarmed_F: LSV_02_unarmed_base_F { RemoveGetIn };

	class LSV_01_armed_base_F;
	class B_T_LSV_01_armed_F: LSV_01_armed_base_F { RemoveGetIn };
	class B_LSV_01_armed_F: LSV_01_armed_base_F { RemoveGetIn };

	class LSV_01_unarmed_base_F;
	class B_T_LSV_01_unarmed_F: LSV_01_unarmed_base_F { RemoveGetIn };
	class B_LSV_01_unarmed_F: LSV_01_unarmed_base_F { RemoveGetIn };

	class LSV_01_light_base_F;
	class B_CTRG_LSV_01_light_F: LSV_01_light_base_F { RemoveGetIn };

	class Quadbike_01_base_F;
	class C_Quadbike_01_F: Quadbike_01_base_F { RemoveGetIn };
	class I_Quadbike_01_F: Quadbike_01_base_F { RemoveGetIn };
	class I_G_Quadbike_01_F: Quadbike_01_base_F { RemoveGetIn };
	class O_G_Quadbike_01_F: I_G_Quadbike_01_F { RemoveGetIn };
	class B_G_Quadbike_01_F: I_G_Quadbike_01_F { RemoveGetIn };
	class O_Quadbike_01_F: Quadbike_01_base_F { RemoveGetIn };
	class O_T_Quadbike_01_ghex_F: Quadbike_01_base_F { RemoveGetIn };
	class B_Quadbike_01_F: Quadbike_01_base_F { RemoveGetIn };
	class B_T_Quadbike_01_F: Quadbike_01_base_F { RemoveGetIn };

	class Offroad_01_armed_base_F;
	class I_G_Offroad_01_armed_F: Offroad_01_armed_base_F { RemoveGetIn };
	class B_G_Offroad_01_armed_F: I_G_Offroad_01_armed_F { RemoveGetIn };
	class O_G_Offroad_01_armed_F: I_G_Offroad_01_armed_F { RemoveGetIn };

	class Offroad_01_civil_base_F;
	class B_GEN_Offroad_01_gen_F: Offroad_01_civil_base_F { RemoveGetIn };
	class C_Offroad_01_F: Offroad_01_civil_base_F { RemoveGetIn };

	class Offroad_01_repair_base_F;
	class C_Offroad_01_repair_F: Offroad_01_repair_base_F { RemoveGetIn };

	class Offroad_01_repair_military_base_F;
	class B_G_Offroad_01_repair_F: Offroad_01_repair_military_base_F { RemoveGetIn };
	class I_G_Offroad_01_repair_F: Offroad_01_repair_military_base_F { RemoveGetIn };
	class O_G_Offroad_01_repair_F: Offroad_01_repair_military_base_F { RemoveGetIn };

	class Offroad_01_military_base_F;
	class I_G_Offroad_01_F: Offroad_01_military_base_F { RemoveGetIn };
	class B_G_Offroad_01_F: I_G_Offroad_01_F { RemoveGetIn };
	class O_G_Offroad_01_F: I_G_Offroad_01_F { RemoveGetIn };

	class C_Kart_01_F_Base;
	class C_Kart_01_F: C_Kart_01_F_Base { RemoveGetIn };
	class C_Kart_01_Blu_F: C_Kart_01_F_Base { RemoveGetIn };
	class C_Kart_01_Red_F: C_Kart_01_F_Base { RemoveGetIn };
	class C_Kart_01_Vrana_F: C_Kart_01_F_Base { RemoveGetIn };

	class Hatchback_01_base_F;
	class C_Hatchback_01_F: Hatchback_01_base_F { RemoveGetIn };
	class C_Hatchback_01_beigecustom_F: C_Hatchback_01_F{ RemoveGetIn };
	class C_Hatchback_01_black_F: C_Hatchback_01_F{ RemoveGetIn };
	class C_Hatchback_01_blue_F: C_Hatchback_01_F{ RemoveGetIn };
	class C_Hatchback_01_bluecustom_F: C_Hatchback_01_F{ RemoveGetIn };
	class C_Hatchback_01_dark_F: C_Hatchback_01_F{ RemoveGetIn };
	class C_Hatchback_01_green_F: C_Hatchback_01_F{ RemoveGetIn };
	class C_Hatchback_01_grey_F: C_Hatchback_01_F{ RemoveGetIn };

	class Hatchback_01_sport_base_F;
	class C_Hatchback_01_sport_F: Hatchback_01_sport_base_F { RemoveGetIn };
	class C_Hatchback_01_sport_blue_F: C_Hatchback_01_sport_F{ RemoveGetIn };
	class C_Hatchback_01_sport_green_F: C_Hatchback_01_sport_F{ RemoveGetIn };
	class C_Hatchback_01_sport_grey_F: C_Hatchback_01_sport_F{ RemoveGetIn };
	class C_Hatchback_01_sport_orange_F: C_Hatchback_01_sport_F{ RemoveGetIn };
	class C_Hatchback_01_sport_red_F: C_Hatchback_01_sport_F{ RemoveGetIn };
	class C_Hatchback_01_sport_white_F: C_Hatchback_01_sport_F{ RemoveGetIn };
	class C_Hatchback_01_white_F: C_Hatchback_01_sport_F{ RemoveGetIn };
	class C_Hatchback_01_yellow_F: C_Hatchback_01_sport_F{ RemoveGetIn };

	class Van_01_transport_base_F;
	class C_Van_01_transport_F: Van_01_transport_base_F { RemoveGetIn };
	class C_Van_01_transport_white_F: C_Van_01_transport_F {};
	class C_Van_01_transport_red_F: C_Van_01_transport_F {};
	class I_C_Van_01_transport_olive_F: C_Van_01_transport_F {};
	class I_G_Van_01_transport_F: Van_01_transport_base_F { RemoveGetIn };
	class B_G_Van_01_transport_F: I_G_Van_01_transport_F { RemoveGetIn };
	class O_G_Van_01_transport_F: I_G_Van_01_transport_F { RemoveGetIn };
	class I_C_Van_01_transport_F: Van_01_transport_base_F { RemoveGetIn };

	class Van_01_box_base_F;
	class C_Van_01_box_F: Van_01_box_base_F { RemoveGetIn };

	class Van_01_fuel_base_F;
	class C_Van_01_fuel_F: Van_01_fuel_base_F { RemoveGetIn };
	class C_Van_01_fuel_white_F: C_Van_01_fuel_F { RemoveGetIn };
	class C_Van_01_fuel_white_v2_F: C_Van_01_fuel_F { RemoveGetIn };
	class C_Van_01_fuel_red_F: C_Van_01_fuel_F { RemoveGetIn };
	class C_Van_01_fuel_red_v2_F: C_Van_01_fuel_F { RemoveGetIn };
	class C_Van_01_box_white_F: C_Van_01_fuel_F { RemoveGetIn };
	class C_Van_01_box_red_F: C_Van_01_fuel_F { RemoveGetIn };
	class I_G_Van_01_fuel_F: Van_01_fuel_base_F { RemoveGetIn };
	class B_G_Van_01_fuel_F: I_G_Van_01_fuel_F { RemoveGetIn };
	class O_G_Van_01_fuel_F: I_G_Van_01_fuel_F { RemoveGetIn };

	class Offroad_02_unarmed_base_F;
	class C_Offroad_02_unarmed_F: Offroad_02_unarmed_base_F { RemoveGetIn };
	class I_C_Offroad_02_unarmed_F: Offroad_02_unarmed_base_F { RemoveGetIn };

	class SUV_01_base_F;
	class C_SUV_01_F: SUV_01_base_F { RemoveGetIn };

	class Truck_02_fuel_base_F;
	class C_Truck_02_fuel_F: Truck_02_fuel_base_F { RemoveGetIn };
	class I_Truck_02_fuel_F: Truck_02_fuel_base_F { RemoveGetIn };
	class O_Truck_02_fuel_F: Truck_02_fuel_base_F { RemoveGetIn };

	class Truck_02_box_base_F;
	class C_Truck_02_box_F: Truck_02_box_base_F { RemoveGetIn };
	class I_Truck_02_box_F: Truck_02_box_base_F { RemoveGetIn };
	class O_Truck_02_box_F: Truck_02_box_base_F { RemoveGetIn };

	class Truck_02_Ammo_base_F;
	class I_Truck_02_ammo_F: Truck_02_Ammo_base_F { RemoveGetIn };
	class O_Truck_02_Ammo_F: Truck_02_Ammo_base_F { RemoveGetIn };

	class Truck_02_base_F;
	class C_Truck_02_covered_F: Truck_02_base_F { RemoveGetIn };
	class I_Truck_02_covered_F: Truck_02_base_F { RemoveGetIn };
	class O_Truck_02_covered_F: Truck_02_base_F { RemoveGetIn };

	class Truck_02_medical_base_F;
	class I_Truck_02_medical_F: Truck_02_medical_base_F { RemoveGetIn };
	class O_Truck_02_medical_F: Truck_02_medical_base_F { RemoveGetIn };

	class Truck_02_transport_base_F;
	class C_Truck_02_transport_F: Truck_02_transport_base_F { RemoveGetIn };
	class I_Truck_02_transport_F: Truck_02_transport_base_F { RemoveGetIn };
	class O_Truck_02_transport_F: Truck_02_transport_base_F { RemoveGetIn };
	
	
	class Truck_01_base_F;
	class B_Truck_01_transport_F: Truck_01_base_F { RemoveGetIn };
	class B_Truck_01_mover_F: B_Truck_01_transport_F { RemoveGetIn };
	class B_T_Truck_01_mover_F: B_Truck_01_mover_F { RemoveGetIn };
	class B_Truck_01_ammo_F: B_Truck_01_mover_F { RemoveGetIn };
	class B_T_Truck_01_ammo_F: B_Truck_01_ammo_F { RemoveGetIn };
	class B_Truck_01_fuel_F: B_Truck_01_mover_F { RemoveGetIn };
	class B_T_Truck_01_fuel_F: B_Truck_01_fuel_F { RemoveGetIn };
	class B_Truck_01_Repair_F: B_Truck_01_mover_F { RemoveGetIn };
	class B_T_Truck_01_Repair_F: B_Truck_01_Repair_F { RemoveGetIn };
	class B_Truck_01_box_F: B_Truck_01_Repair_F { RemoveGetIn };
	class B_Truck_01_medical_F: B_Truck_01_transport_F { RemoveGetIn };
	class B_T_Truck_01_medical_F: B_Truck_01_medical_F { RemoveGetIn };
	class B_T_Truck_01_transport_F: B_Truck_01_transport_F { RemoveGetIn };
	class B_Truck_01_covered_F: B_Truck_01_transport_F { RemoveGetIn };
	class B_T_Truck_01_covered_F: B_Truck_01_covered_F { RemoveGetIn };

	class Truck_03_base_F;
	class O_Truck_03_device_F: Truck_03_base_F { RemoveGetIn };
	class O_T_Truck_03_device_ghex_F: O_Truck_03_device_F { RemoveGetIn };
	class O_Truck_03_ammo_F: Truck_03_base_F { RemoveGetIn };
	class O_T_Truck_03_ammo_ghex_F: O_Truck_03_ammo_F { RemoveGetIn };
	class O_Truck_03_fuel_F: Truck_03_base_F { RemoveGetIn };
	class O_T_Truck_03_fuel_ghex_F: O_Truck_03_fuel_F { RemoveGetIn };
	class O_Truck_03_repair_F: Truck_03_base_F { RemoveGetIn };
	class O_T_Truck_03_repair_ghex_F: O_Truck_03_repair_F { RemoveGetIn };
	class O_Truck_03_medical_F: Truck_03_base_F { RemoveGetIn };
	class O_T_Truck_03_medical_ghex_F: O_Truck_03_medical_F { RemoveGetIn };
	class O_Truck_03_transport_F: Truck_03_base_F { RemoveGetIn };
	class O_T_Truck_03_transport_ghex_F: O_Truck_03_transport_F { RemoveGetIn };
	class O_Truck_03_covered_F: Truck_03_base_F { RemoveGetIn };
	class O_T_Truck_03_covered_ghex_F: O_Truck_03_covered_F { RemoveGetIn };

	class B_MBT_01_arty_base_F;
	class B_MBT_01_arty_F: B_MBT_01_arty_base_F { RemoveGetIn };
	class B_T_MBT_01_arty_F: B_MBT_01_arty_F { RemoveGetIn };

	class B_MBT_01_mlrs_base_F;
	class B_MBT_01_mlrs_F: B_MBT_01_mlrs_base_F { RemoveGetIn };
	class B_T_MBT_01_mlrs_F: B_MBT_01_mlrs_F { RemoveGetIn };

	class B_MBT_01_base_F;
	class B_MBT_01_cannon_F: B_MBT_01_base_F { RemoveGetIn };
	class B_T_MBT_01_cannon_F: B_MBT_01_cannon_F { RemoveGetIn };
	class B_MBT_01_TUSK_F: B_MBT_01_cannon_F { RemoveGetIn };
	class B_T_MBT_01_TUSK_F: B_MBT_01_TUSK_F { RemoveGetIn };

	class O_MBT_02_base_F;
	class O_MBT_02_cannon_F: O_MBT_02_base_F { RemoveGetIn };
	class O_T_MBT_02_cannon_ghex_F: O_MBT_02_cannon_F { RemoveGetIn };
	
	class O_MBT_02_arty_base_F;
	class O_MBT_02_arty_F: O_MBT_02_arty_base_F { RemoveGetIn };
	class O_T_MBT_02_arty_ghex_F: O_MBT_02_arty_F { RemoveGetIn };

	class I_MBT_03_base_F;
	class I_MBT_03_cannon_F: I_MBT_03_base_F { RemoveGetIn };
};

