class CfgPatches
{
	class DS_OBE_Layer_CUP
	{
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"DS_OBE_Layer"};
	};
};



/////////////////////////////////////////////////////
////////////////// LAYER DATA ///////////////////////
/////////////////////////////////////////////////////

class CfgOBE
{
	class CLASSNAME_1
	{
		positions[] =
		{
			{-0.929199,0.090332,-0.248512},		//Driver
			{0.854492,-0.00341797,-0.256302},	//CoDriver
			{0.897949,-1.16919,-0.197901},		//Back Right
			{-0.984375,-1.06177,-0.183472}		//Back Left
		};
		position_infos[] =
		{
			{"drv",		"getindriver_ca",	{},				"STR_OBE_VehPos_Driver"},
			{"crg",		"getincargo_ca",	{0},			"STR_OBE_VehPos_CoDriver"},
			{"crg",		"getincargo_ca",	{1},			"STR_OBE_VehPos_Backseat_Right"},
			{"crg",		"getincargo_ca",	{2},			"STR_OBE_VehPos_Backseat_Left"}
		};
	};
	class CLASSNAME_2: CLASSNAME_1{};
};

/*
Icons

Driver		=	getindriver_ca
Pilot		=	getinpilot_ca
Gunner		=	getingunner_ca
Commander	=	getincommander_ca
Cargo		=	getincargo_ca

Other available IconsPath:
a3\ui_f\data\IGUI\Cfg\Actions\NameOfDesiredIcon.paa
( e.g. getincargo_ca -> "a3\ui_f\data\IGUI\Cfg\Actions\getincargo_ca.paa" )


Stringtable Available Names:

//Mainseats
	"STR_OBE_VehPos_Driver"
	"STR_OBE_VehPos_Pilot"
	"STR_OBE_VehPos_CoDriver"
	"STR_OBE_VehPos_CoPilot"
	"STR_OBE_VehPos_Commander"
	"STR_OBE_VehPos_Loadmaster"
	
//Gunner Pos
	"STR_OBE_VehPos_Gunner"
	"STR_OBE_VehPos_Gunner_Right"
	"STR_OBE_VehPos_Gunner_Center"
	"STR_OBE_VehPos_Gunner_Left"
	"STR_OBE_VehPos_Gunner_Door"
	"STR_OBE_VehPos_Gunner_Window"
	"STR_OBE_VehPos_Gunner_Rear"
	"STR_OBE_VehPos_Gunner_Rear_Right"
	"STR_OBE_VehPos_Gunner_Rear_Left"
	
//Backseat - Singular	
	"STR_OBE_VehPos_Backseat"
	"STR_OBE_VehPos_Backseat_Right"
	"STR_OBE_VehPos_Backseat_Left"
	"STR_OBE_VehPos_Backseat_Front"
	"STR_OBE_VehPos_Backseat_Center"
	"STR_OBE_VehPos_Backseat_Rear"
	
//Backseat - Plural
	"STR_OBE_VehPos_Backseats"
	"STR_OBE_VehPos_Backseats_Right"
	"STR_OBE_VehPos_Backseats_Left"
	"STR_OBE_VehPos_Backseats_Front"
	"STR_OBE_VehPos_Backseats_Center"
	"STR_OBE_VehPos_Backseats_Rear"
	
//Seat - Singular
	"STR_OBE_VehPos_Seat"
	"STR_OBE_VehPos_Seat_Cockpit_Right"
	"STR_OBE_VehPos_Seat_Cockpit_Left"
	"STR_OBE_VehPos_Seat_Right"
	"STR_OBE_VehPos_Seat_Left"
	"STR_OBE_VehPos_Seat_Front"
	"STR_OBE_VehPos_Seat_Center"
	"STR_OBE_VehPos_Seat_Rear"
	
//Seat - Plural
	"STR_OBE_VehPos_Seats"
	"STR_OBE_VehPos_Seats_Cockpit_Right"
	"STR_OBE_VehPos_Seats_Cockpit_Left"
	"STR_OBE_VehPos_Seats_Right"
	"STR_OBE_VehPos_Seats_Left"
	"STR_OBE_VehPos_Seats_Front"
	"STR_OBE_VehPos_Seats_Center"
	"STR_OBE_VehPos_Seats_Rear"
	
//Flatbed (e.g. in Taru-Medic)
	"STR_OBE_VehPos_Flatbed"
	"STR_OBE_VehPos_Flatbed_Right"
	"STR_OBE_VehPos_Flatbed_Left"
	"STR_OBE_VehPos_Flatbed_Front"
	"STR_OBE_VehPos_Flatbed_Center"
	"STR_OBE_VehPos_Flatbed_Rear"
*/



/////////////////////////////////////////////////////
//////////////// CONFIG CHANGES /////////////////////
/////////////////////////////////////////////////////
#define RemoveGetIn getInRadius = 0.0001;
class CfgVehicles
{
	
	class CLASSNAME_1;
	class CLASSNAME_2: CLASSNAME_1 { RemoveGetIn };
};

